import { AuthContext, getUser } from './Context/AuthContext';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Header from './components/Base/Header/Header';
import NotFound from './components/Pages/NotFound/NotFound';
import React, { useState } from 'react';
import './App.css';
import Profile from './components/Pages/Profile/Profile';
import ForgotPassword from './components/Pages/ForgotPassword/ForgotPassword';
import ResetPassword from './components/Pages/ResetPassword/ResetPassword';
import Dashboard from './components/Pages/Dashboard/Dashboard';
import Login from './components/Pages/Login/Login';
import Home from './components/Pages/Home/Home';
import ServicesUser from './components/Pages/Services/ServicesUser';

const App = () => {
  const [authValue, setAuthState] = useState({
    user: getUser(),
    isLoggedIn: Boolean(getUser()),
  });

  return (
    <div className="App">
      <BrowserRouter>
        <AuthContext.Provider
          value={{ ...authValue, setAuthState }}
        >
          <Header />
          <Switch>
          <Redirect path="/" exact to="/home"  />
            <Route path="/home"  component={Home} />
            <Route path="/login" isLoggedIn={!authValue.isLoggedIn} exact component={Login} />
            <Route path="/forgot"  exact component={ForgotPassword} />
            <Route path="/reset" isLoggedIn={!authValue.isLoggedIn} exact component={ResetPassword} />
            <Route path="/dashboard" isLoggedIn={authValue.isLoggedIn} exact component={Dashboard} />
            <Route path="/profile" isLoggedIn={authValue.isLoggedIn} exact to component={Profile} />
            <Route path="/services" isLoggedIn={authValue.isLoggedIn} exact to component={ServicesUser} />
            <Route path="*" component={NotFound} />
          </Switch>
        </AuthContext.Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
