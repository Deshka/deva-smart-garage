import * as Yup from 'yup';

export const validationSchemaLogin = Yup.object().shape({
  email: Yup.string().email("Please enter a valid email!").required("Required"),
  password: Yup.string().min(8, 'Please, insert min length 8 chars!').max(20, 'Please, insert max length 10 chars!').required("Required"),
});

export const validationSchemaForgot = Yup.object().shape({
  email: Yup.string().email("Please enter an email you have registered with!").required("Required"),
});

export const validationSchemaReset = Yup.object().shape({
  password: Yup.string().min(8, "Password should be at least 8 symbols long").required('This field is required'),
  passwordConfirmation: Yup.mixed().test(
    "match",
    "Passwords do not match",
    function () {
      return this.parent.password === this.parent.passwordConfirmation;
    }
  )
});

