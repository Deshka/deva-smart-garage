import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import Fade from "react-reveal/Fade";
import { useStylesHome } from "./Home-styles";

const Home = () => {
  const [spacing, setSpacing] = useState(2);
  const classes = useStylesHome();

  return (
    <div className={classes.root}>
      <Grid container className={classes.root} spacing={2}>
        <Grid item xs={12}>
          <Grid container justify="center" spacing={spacing}>
            <Fade top>
              <Grid item>
                <h1>Welcome To Deva Smart Garage</h1>
                <img
                  alt="Porsche"
                  src="http://localhost:3000/11.jpg"
                  width="100%"
                />
              </Grid>
            </Fade>
            <Fade right>
              <Grid item>
                <h1>Our Services</h1>
                <img
                  alt="Porsche"
                  src="http://localhost:3000/4.jpg"
                  width="75%"
                />
              </Grid>
            </Fade>
            <Fade left>
              <Grid item>
                <img
                  src="http://localhost:3000/6.jpg"
                  width="75%"
                />
              </Grid>
            </Fade>
          </Grid>
        </Grid>
        <Grid item xs={12}></Grid>
      </Grid>
    </div>
  );
};

export default Home;
