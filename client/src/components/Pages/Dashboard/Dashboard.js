import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { useStylesDashboard } from './styles/Dashboard-styles';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import CssBaseline from '@material-ui/core/CssBaseline';
import BarChartIcon from '@material-ui/icons/BarChart';
import IconButton from '@material-ui/core/IconButton';
import PeopleIcon from '@material-ui/icons/People';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import Footer from '../../Base/Footer/Footer';
import Drawer from '@material-ui/core/Drawer';
import Services from './Services';
import List from '@material-ui/core/List';
import React, { useState } from 'react';
import Orders from './Orders';
import Image from 'material-ui-image';
import Customers from './Customers';
import Vehicles from './Vehicles';
import clsx from 'clsx';

const Dashboard = () => {
  const classes = useStylesDashboard();
  const [open, setOpen] = useState(true);
  const [isClickedVehicles, setIsClickedVehicles] = useState(false)
  const [isClickedCustomers, setIsClickedCustomers] = useState(false)
  const [isClickedServices, setIsClickedServices] = useState(false)
  const [isClickedOrders, setIsClickedOrders] = useState(false)

  const toggleVehicles = () => {
    isClickedVehicles ? setIsClickedVehicles(false) : setIsClickedVehicles(true)
    setIsClickedCustomers(false)
    setIsClickedServices(false)
    setIsClickedOrders(false)
  }

  const toggleCustomers = () => {
    isClickedCustomers ? setIsClickedCustomers(false) : setIsClickedCustomers(true)
    setIsClickedVehicles(false)
    setIsClickedServices(false)
    setIsClickedOrders(false)
  }

  const toggleServices = () => {
    isClickedServices ? setIsClickedServices(false) : setIsClickedServices(true)
    setIsClickedVehicles(false)
    setIsClickedCustomers(false)
    setIsClickedOrders(false)
  }

  const toggleOrders = () => {
    isClickedOrders ? setIsClickedOrders(false) : setIsClickedOrders(true)
    setIsClickedVehicles(false)
    setIsClickedCustomers(false)
    setIsClickedServices(false)
  }

  const handleDrawer = () => {
    open ? setOpen(false) : setOpen(true)
  }

  return (
    <div style={{ backgroundImage: `url('http://localhost:3000/home.gif')`, width: '100%', height: '100%' }} className={classes.root} >
      <Image
        src='http://localhost:3000/home.gif'
      />
      <CssBaseline />
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawer}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider style={{ padding: 3 }} />
        <List>
          <ListItem button onClick={toggleVehicles}>
            <ListItemIcon>
              <DashboardIcon />
            </ListItemIcon>
            <ListItemText primary="Vehicles" />
          </ListItem >
          <ListItem button onClick={toggleOrders}>
            <ListItemIcon>
              <ShoppingCartIcon />
            </ListItemIcon>
            <ListItemText primary="Orders" />
          </ListItem>
          <ListItem button onClick={toggleCustomers}>
            <ListItemIcon>
              <PeopleIcon />
            </ListItemIcon>
            <ListItemText primary="Customers" />
          </ListItem>
          <ListItem button onClick={toggleServices} >
            <ListItemIcon>
              <BarChartIcon />
            </ListItemIcon>
            <ListItemText primary="Services" />
          </ListItem>
        </List>
        <Divider style={{ padding: 3 }} />
      </Drawer>
      {isClickedVehicles ? <Vehicles /> : ''}
      {isClickedCustomers ? <Customers /> : ''}
      {isClickedServices ? <Services /> : ''}
      {isClickedOrders ? <Orders /> : ''}
      <Footer />
    </div>
  );
}

export default Dashboard;
