/* eslint-disable react/display-name */
import { BASE_URL_AVATARS_DEFAULT } from '../../../common/constants';
import { tableIcons } from "../../../common/material-table-icons";
import { ToastContainer, toast, Bounce } from 'react-toastify'
import { BASE_URL } from "../../../common/constants";
import React, { useState, useEffect } from "react";
import 'react-toastify/dist/ReactToastify.css';
import MaterialTable from "material-table";

const Customers = () => {

  const columns = [
    { title: "Avatar", field: "avatar", editable: false, render: rowData => <img src={rowData.avatar} style={{ width: 50, borderRadius: '50%' }} /> },
    { title: "First Name", field: "first_name" },
    { title: "Last Name", field: "last_name" },
    { title: "E-mail", field: "email" },
    { title: "Phone number", field: "phone_number" },
    { title: "Address", field: "street_address" },
    { title: "City", field: "city" },
    { title: "Country", field: "country" },
    { title: "Post code", field: "postal_code" },
  ];

  const [rows, setRows] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [isClicked, setIsClicked] = useState(false);

  useEffect(() => {
    setRows(customers);
  }, [customers]);

  const getAllCustomers = () => {
    fetch(`${BASE_URL}/users`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          toast.error(`${res.message}`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        } else {
          setCustomers(result);
        }
      })
      .catch((error) => console.log(error.message));
  };

  useEffect(() => {
    getAllCustomers();
  }, [isClicked]);

  const createUser = (newUser, updatedRows) => {
    fetch(`${BASE_URL}/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(newUser),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.message) {
          toast.error(`${result.message}`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        } else {
          setRows(updatedRows);
          toast.success(`You successfully create a customer!`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        }
      })
      .then(() => setIsClicked(true))
  };

  const updateUser = (id, updatedUser, dataUpdate) => {
    fetch(`${BASE_URL}/users/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(updatedUser),
    })
      .then((result) => result.json())
      .then((result) => {
        if (result.error) {
          toast.error(`${result.message}`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        } else {
          // setRows([...dataUpdate])
          toast.success(`You successfully updated customer!`, {
            position: toast.POSITION.TOP_CENTER,
            transition: Bounce,
            autoClose: true,
          })
        }
      })
      .then(() => setIsClicked(true))
  };

  const deleteUser = (id) => {
    fetch(`${BASE_URL}/users/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((result) => result.json())
      .then(() => setIsClicked(true))
      .catch((error) => console.log(error))
  };

  return (
    <div>
      <ToastContainer />
      <MaterialTable
        icons={tableIcons}
        title="Customers"
        columns={columns}
        data={rows}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              const newUser = {
                first_name: newData.first_name,
                last_name: newData.last_name,
                email: newData.email,
                phone_number: newData.phone_number,
                role: newData.role,
                postal_code: newData.postal_code,
                country: newData.country,
                city: newData.city,
                street_address: newData.street_address,
              };
              const updatedRows = [...rows, { ...newData, avatar: `${BASE_URL_AVATARS_DEFAULT}` }]
              setTimeout(() => {
                createUser(newUser, updatedRows);
                setRows(updatedRows);
                resolve();
              }, 1000);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                const updatedUser = {
                  first_name: newData.first_name,
                  last_name: newData.last_name,
                  email: newData.email,
                  phone_number: newData.phone_number,
                  role: newData.role,
                  postal_code: newData.postal_code,
                  country: newData.country,
                  city: newData.city,
                  street_address: newData.street_address,
                  avatar: newData.avatar,

                };
                const dataUpdate = [...rows];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                updateUser(newData.id, updatedUser, dataUpdate);
                setRows([...dataUpdate]);
                resolve();
              }, 1000);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                deleteUser(oldData.id);
                const dataDelete = [...rows];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1);
                setRows([...dataDelete]);
                resolve();
              }, 1000);
            }),
        }}
        options={{
          filtering: true,
          addRowPosition: 'first',
          tableLayout: "auto",
          padding: "dense",
          pageSize: 10,
          doubleHorizontalScroll: false,
          headerStyle: {
            width: 26,
            whiteSpace: "nowrap",
            textAlign: "left",
            flexDirection: "row",
            overflow: "hidden",
            textOverflow: "ellipsis",
            paddingLeft: 5,
            paddingRight: 5,
            backgroundColor: "#f50057",
            color: "whitesmoke",
            fontWeight: "bold",
            fontSize: 16,
          },
          rowStyle: { backgroundColor: "#E5E5E5" },
        }}
      />
    </div>
  );
};

export default Customers;
