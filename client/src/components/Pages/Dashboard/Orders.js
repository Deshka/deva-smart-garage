import React, { useState, useEffect } from "react";
import { BASE_URL } from "../../../common/constants";
import MaterialTable from "material-table";
import { tableIcons } from "../../../common/material-table-icons";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { Typography } from "@material-ui/core";
import { status } from "../../../common/status";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import { makeStyles } from "@material-ui/core/styles";


const Orders = () => {
  const columns = [
    { field: "fullname", title: "Customer", minWidth: 100, editable: "never" },
    { field: "vin", title: "VIN", minWidth: 170, editable: "never" },
    { field: "brand_name", title: "Brand", editable: "never" },
    { field: "model_name", title: "Model", editable: "never" },
    {
      field: "issue_date",
      title: "Issue Date",
      type: "date",
      dateSetting: { locale: "en-US" },
      editable: "never",
    },
    {
      field: "due_date",
      title: "Due Date",
      type: "date",
      dateSetting: { locale: "en-US" },
      editable: "never",
    },
    { field: "service_name", title: "Services", editable: "never" },
    { field: "sum", title: "Total Sum €", editable: "never" },
    {
      field: "status",
      title: "Status",
      lookup: { new: "new", processing: "processing", done: "done" },
    },
    { field: "notes", title: "Notes", editable: "never" },
    { field: "employee_full_name", title: "Done By", editable: "never" },
  ];
  const useStyles = makeStyles((theme) => ({
    root: {
      width: "100%",
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
  }));
  const classes = useStyles();
  const [rows, setRows] = useState([]);
  const [orders, setOrders] = useState([]);
  const [isClicked, setIsClicked] = useState(false);
  const [open, setOpen] = useState(false);
  const [openService, setOpenService] = useState(false);
  const [customers, setCustomers] = useState([]);
  const [services, setServices] = useState([]);
  const [vehicleId, setVehicleId] = useState(-1);
  const [newOrderId, setNewOrderId] = useState(-1);
  const [checked, setChecked] = useState([]);
  const [newCustomerId, setNewCustomerId] = useState(-1);

  useEffect(() => {
    getAllOrders();
  }, [isClicked]);

  useEffect(() => {
    setRows(orders);
  }, [orders]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    updateOrder(newOrderId, value.id);
    const newChecked = [...checked];
    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
  };

  const [newOrder, setNewOrder] = useState({
    due_date: "",
    issue_date: "",
    notes: "",
    status: "",
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = () => {
    handleOpenService();
  };

  const handleOpenService = () => {
    setIsClicked(false);
    setOpen(false);
    createOrder(vehicleId, newOrder);
    setOpenService(true);
  };

  const handleCloseService = () => {
    setOpenService(false);
    setIsClicked(true);
    setChecked([0]);
  };

  const createOrderProps = (prop, value) => {
    newOrder[prop] = value;
    const created = { ...newOrder };
    setNewOrder(created);
  };

  useEffect(() => {
    fetch(`${BASE_URL}/users`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        const arrCustomers = Object.values(result).map(
          ({ vehicle_id, full_name, id }) => [vehicle_id, full_name, id]
        );
        setCustomers(arrCustomers);
      })
      .catch((error) => console.log(error.message));
  }, []);

  useEffect(() => {
    fetch(`${BASE_URL}/services`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        setServices(result);
      })
      .catch((error) => console.log(error.message));
  }, []);
  const getAllOrders = () => {
    fetch(`${BASE_URL}/orders`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        setOrders(result);
      })
      .catch((error) => console.log(error.message));
  };

  const updateOrderStatus = (ordId, value) => {
    fetch(`${BASE_URL}/order/${ordId}/status`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({ status: value }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        
      })
      .catch((error) => console.log(error.message));
  };

  const updateOrder = (ordId, value) => {
    fetch(`${BASE_URL}/user/order/${ordId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({ service_id: value }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
      })
      .catch((error) => console.log(error.message));
  };

  const createOrder = (id, newOrder) => {
    fetch(`${BASE_URL}/order/vehicle/${id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(newOrder),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        const order = Object.values(result).map(({ id }) => id);
        setNewOrderId(...order);

        setNewOrder({
          due_date: "",
          issue_date: "",
          notes: "",
          status: "",
        });
      })
      .catch((error) => console.log(error.message));
  };

  const deleteOrder = (ordId) => {
    fetch(`${BASE_URL}/user/order/${ordId}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + `${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
      });
  };

  return (
    <div>
      <Button variant="outlined" color="secondary" onClick={handleClickOpen}>
        Add an Order
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title" style={{ textAlign: "center" }}>
          Create Order
        </DialogTitle>
        <DialogContent>
          <DialogContentText style={{ textAlign: "center" }}>
            Please, fill all the fields to create a new order!
          </DialogContentText>
          Issue Date:
          <TextField
            autoFocus
            margin="dense"
            id="issue_date"
            type="date"
            fullWidth
            required
            value={newOrder.issue_date}
            onChange={(e) => createOrderProps("issue_date", e.target.value)}
          />
          Due Date:
          <TextField
            autoFocus
            margin="dense"
            id="due_date"
            type="date"
            required
            fullWidth
            value={newOrder.due_date}
            onChange={(e) => createOrderProps("due_date", e.target.value)}
          />
          Notes:
          <TextField
            autoFocus
            margin="dense"
            id="notes"
            fullWidth
            required
            label="Required"
            value={newOrder.notes}
            onChange={(e) => createOrderProps("notes", e.target.value)}
          />
          <Typography>
            Customers:
            <Select
              style={{ width: 200, textAlign: "center" }}
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              helperText="Select User!"
              onChange={(e) => setVehicleId(e.target.value)}
            >
              {customers.map((customer, index) => (
                <MenuItem key={index} value={customer[0]}>
                  {customer[1]}
                </MenuItem>
              ))}
            </Select>
          </Typography>
          <Typography style={{ paddingRight: 25 }}>
            Status:
            <Select
              style={{ width: 200, textAlign: "center" }}
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              helperText="Select status!"
              placeholder="Status"
              required
              label="Required"
              value={newOrder.status}
              onChange={(e) => createOrderProps("status", e.target.value)}
            >
              {status.map((st, index) => (
                <MenuItem key={index} value={st}>
                  {st}
                </MenuItem>
              ))}
            </Select>
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button onClick={(handleSubmit, handleOpenService)} color="secondary">
            Next
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openService}
        onClose={handleCloseService}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="other" style={{ textAlign: "center" }}>
          Add Services
        </DialogTitle>
        <List dense className={classes.root}>
          {services.map((value) => {
            const labelId = `checkbox-list-secondary-label-${value}`;
            return (
              <ListItem key={value.id} button>
                <ListItemText
                  id={value.id}
                  primary={`${value.name} ${value.price}`}
                />
                <ListItemSecondaryAction>
                  <Checkbox
                    edge="end"
                    onChange={handleToggle(value)}
                    checked={checked.indexOf(value) !== -1}
                    inputProps={{ "aria-labelledby": labelId }}
                  />
                </ListItemSecondaryAction>
              </ListItem>
            );
          })}
        </List>
        <DialogActions>
          <Button onClick={handleCloseService} color="secondary">
            Cancel
          </Button>
          <Button onClick={handleCloseService} color="secondary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
      <MaterialTable
        icons={tableIcons}
        title="Orders"
        columns={columns}
        data={rows}
        editable={{
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                updateOrderStatus(oldData.order_id, newData.status);
                const dataUpdate = [...rows];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                setRows([...dataUpdate]);
                resolve();
              }, 1000);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                deleteOrder(oldData.order_id);
                const dataDelete = [...rows];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1);
                setRows([...dataDelete]);
                resolve();
              }, 1000);
            }),
        }}
        options={{
          filtering: true,
          addRowPosition: "first",
          tableLayout: "auto",
          padding: "dense",
          pageSize: 10,
          doubleHorizontalScroll: false,
          width: 56,
          headerStyle: {
            width: 56,
            whiteSpace: "nowrap",
            textAlign: "left",
            flexDirection: "row",
            overflow: "hidden",
            textOverflow: "ellipsis",
            paddingLeft: 5,
            paddingRight: 5,
            backgroundColor: "#f50057",
            color: "whitesmoke",
            fontWeight: "bold",
            fontSize: 16,
            alignContent: "center",
          },
          rowStyle: { backgroundColor: "#E5E5E5", width: 56 },
        }}
      />
    </div>
  );
};
export default Orders;
