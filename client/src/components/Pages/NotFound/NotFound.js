import React from "react";
import "./NotFound.css";

const NotFound = () => {
  return (
    <div className="NotFound">
      <img
        src="https://kfg6bckb.media.zestyio.com/yalantis-interactive-404.gif"
        alt="Page not found! :("
      />
    </div>
  );
};

export default NotFound;
