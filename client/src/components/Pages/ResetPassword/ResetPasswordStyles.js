import { makeStyles } from "@material-ui/core/styles";

export const useStylesResetPassword = makeStyles((theme) => ({
  root: {
    marginTop:"100px",
    marginLeft:"180px",
    justifyContent:"center",
     width:"80%"
  },
  paper: {
    margin: theme.spacing(10, 10),
    display: "inline-flex",
    flexDirection: "column",

  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    display: "center",

  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },

    button: {
      margin: theme.spacing(3),
      
    },
}));
