import React from "react";
import "./Footer.css";

const Footer = () => {

  return (
      <div className="footer">
            <label className="copyright">Copyright © DEVA Library 2021.</label>
      </div>
  );
};

export default Footer;
