-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for Win64 (AMD64)
--
-- Host: triton.rdb.superhosting.bg    Database: fwraptea_garage123
-- ------------------------------------------------------
-- Server version	10.3.29-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street_address` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `country` varchar(45) NOT NULL,
  `postal_code` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (1,'Elenovo','Blagoevgrad','Bulgaria','2700'),(2,'Sofia','Patriarh Evtimi','Bulgaria','1229'),(3,'Elenovo','Blagoevgrad','Bulgaria','2700'),(4,'Bolyarka','Sofia','Bulgaria','1011'),(5,'Bolyarka','Sofia','Bulgaria','1011'),(6,'Druzba 190','Pleven','Bulgaria','5800'),(7,'Druzba 190','Pleven','Bulgaria','5800'),(8,'Druzba 190','Pleven','Bulgaria','5800'),(9,'Druzba 190','Pleven','Bulgaria','5800'),(10,'Druzba 190','Pleven','Bulgaria','5800'),(11,'Druzba 190','Pleven','Bulgaria','5800'),(12,'Druzba 190','Pleven','Bulgaria','5800'),(13,'Test Street','Sofia','Bulgaria','1000'),(14,'Test Street','Sofia','Bulgaria','1000'),(15,'Test Street','Sofia','Bulgaria','1000'),(16,'Test Street','Sofia','Bulgaria','1000'),(17,'Rilska','Blagoevgrad','Buulgaria','2700'),(18,'Rilska','Blagoevgrad','Bulgaria','2700'),(19,'Neofit','Sofia','Bulgaria','1000'),(20,'Riletz','Sofia','Bulgaria','1000'),(21,'Hristo Boyev 34','Sofia','Bulgaria','1011'),(22,'Neofiit Rilski','Burgas','Bulgaria','8000'),(23,'Rilska','Nulgar','Bulgaria','8000'),(24,'Riletz','Burgas','Bulgaria','8000'),(25,'Botev 45','Burgas','Bulgaria','8000'),(26,'RIlets','Burgas','Bulgaria','8000'),(27,'Elenka','Sofia','Bulgaria','1011'),(28,'Levski 33','Butgas','Bulgaria','8000'),(29,'Aleksandar Str','Sofia','Bulgaria','1000');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'Seat'),(2,'Renault'),(3,'Peugeot'),(4,'Dacia'),(5,'Citroën'),(6,'Opel'),(7,'Alfa Romeo'),(8,'Škoda'),(9,'Chevrolet'),(10,'Porsche'),(11,'Honda'),(12,'Subaru'),(13,'Mazda'),(14,'Mitsubishi'),(15,'Lexus'),(16,'Toyota'),(17,'BMW'),(18,'Volkswagen'),(19,'Suzuki'),(20,'Mercedes-Benz'),(21,'Saab'),(22,'Audi'),(23,'Kia'),(24,'Land Rover'),(25,'Dodge'),(26,'Chrysler'),(27,'Ford'),(28,'Hummer'),(29,'Hyundai'),(30,'Infiniti'),(31,'Jaguar'),(32,'Jeep'),(33,'Nissan'),(34,'Volvo'),(35,'Daewoo'),(36,'Fiat'),(37,'MINI'),(38,'Rover'),(39,'Smart');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_date` date NOT NULL DEFAULT current_timestamp(),
  `due_date` date NOT NULL,
  `number` int(11) NOT NULL,
  `orders_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_invoices_orders1_idx` (`orders_id`),
  CONSTRAINT `fk_invoices_orders1` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `models`
--

DROP TABLE IF EXISTS `models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(100) NOT NULL,
  `brand_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_models_manufacturers_idx` (`brand_id`) USING BTREE,
  CONSTRAINT `FK_models_brands` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=967 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `models`
--

LOCK TABLES `models` WRITE;
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` VALUES (1,'Alhambra',1),(2,'Altea',1),(3,'Altea XL',1),(4,'Arosa',1),(5,'Cordoba',1),(6,'Cordoba Vario',1),(7,'Exeo',1),(8,'Ibiza',1),(9,'Ibiza ST',1),(10,'Exeo ST',1),(11,'Leon',1),(12,'Leon ST',1),(13,'Inca',1),(14,'Mii',1),(15,'Toledo',1),(16,'Captur',2),(17,'Clio',2),(18,'Clio Grandtour',2),(19,'Espace',2),(20,'Express',2),(21,'Fluence',2),(22,'Grand Espace',2),(23,'Grand Modus',2),(24,'Grand Scenic',2),(25,'Kadjar',2),(26,'Kangoo',2),(27,'Kangoo Express',2),(28,'Koleos',2),(29,'Laguna',2),(30,'Laguna Grandtour',2),(31,'Latitude',2),(32,'Mascott',2),(33,'Mégane',2),(34,'Mégane CC',2),(35,'Mégane Combi',2),(36,'Mégane Grandtour',2),(37,'Mégane Coupé',2),(38,'Mégane Scénic',2),(39,'Scénic',2),(40,'Talisman',2),(41,'Talisman Grandtour',2),(42,'Thalia',2),(43,'Twingo',2),(44,'Wind',2),(45,'Zoé',2),(46,'1007',3),(47,'107',3),(48,'106',3),(49,'108',3),(50,'2008',3),(51,'205',3),(52,'205 Cabrio',3),(53,'206',3),(54,'206 CC',3),(55,'206 SW',3),(56,'207',3),(57,'207 CC',3),(58,'207 SW',3),(59,'306',3),(60,'307',3),(61,'307 CC',3),(62,'307 SW',3),(63,'308',3),(64,'308 CC',3),(65,'308 SW',3),(66,'309',3),(67,'4007',3),(68,'4008',3),(69,'405',3),(70,'406',3),(71,'407',3),(72,'407 SW',3),(73,'5008',3),(74,'508',3),(75,'508 SW',3),(76,'605',3),(77,'806',3),(78,'607',3),(79,'807',3),(80,'Bipper',3),(81,'RCZ',3),(82,'Dokker',4),(83,'Duster',4),(84,'Lodgy',4),(85,'Logan',4),(86,'Logan MCV',4),(87,'Logan Van',4),(88,'Sandero',4),(89,'Solenza',4),(90,'Berlingo',5),(91,'C-Crosser',5),(92,'C-Elissée',5),(93,'C-Zero',5),(94,'C1',5),(95,'C2',5),(96,'C3',5),(97,'C3 Picasso',5),(98,'C4',5),(99,'C4 Aircross',5),(100,'C4 Cactus',5),(101,'C4 Coupé',5),(102,'C4 Grand Picasso',5),(103,'C4 Sedan',5),(104,'C5',5),(105,'C5 Break',5),(106,'C5 Tourer',5),(107,'C6',5),(108,'C8',5),(109,'DS3',5),(110,'DS4',5),(111,'DS5',5),(112,'Evasion',5),(113,'Jumper',5),(114,'Jumpy',5),(115,'Saxo',5),(116,'Nemo',5),(117,'Xantia',5),(118,'Xsara',5),(119,'Agila',6),(120,'Ampera',6),(121,'Antara',6),(122,'Astra',6),(123,'Astra cabrio',6),(124,'Astra caravan',6),(125,'Astra coupé',6),(126,'Calibra',6),(127,'Campo',6),(128,'Cascada',6),(129,'Corsa',6),(130,'Frontera',6),(131,'Insignia',6),(132,'Insignia kombi',6),(133,'Kadett',6),(134,'Meriva',6),(135,'Mokka',6),(136,'Movano',6),(137,'Omega',6),(138,'Signum',6),(139,'Vectra',6),(140,'Vectra Caravan',6),(141,'Vivaro',6),(142,'Vivaro Kombi',6),(143,'Zafira',6),(144,'145',7),(145,'146',7),(146,'147',7),(147,'155',7),(148,'156',7),(149,'156 Sportwagon',7),(150,'159',7),(151,'159 Sportwagon',7),(152,'164',7),(153,'166',7),(154,'4C',7),(155,'Brera',7),(156,'GTV',7),(157,'MiTo',7),(158,'Crosswagon',7),(159,'Spider',7),(160,'GT',7),(161,'Giulietta',7),(162,'Giulia',7),(163,'Favorit',8),(164,'Felicia',8),(165,'Citigo',8),(166,'Fabia',8),(167,'Fabia Combi',8),(168,'Fabia Sedan',8),(169,'Felicia Combi',8),(170,'Octavia',8),(171,'Octavia Combi',8),(172,'Roomster',8),(173,'Yeti',8),(174,'Rapid',8),(175,'Rapid Spaceback',8),(176,'Superb',8),(177,'Superb Combi',8),(178,'Alero',9),(179,'Aveo',9),(180,'Camaro',9),(181,'Captiva',9),(182,'Corvette',9),(183,'Cruze',9),(184,'Cruze SW',9),(185,'Epica',9),(186,'Equinox',9),(187,'Evanda',9),(188,'HHR',9),(189,'Kalos',9),(190,'Lacetti',9),(191,'Lacetti SW',9),(192,'Lumina',9),(193,'Malibu',9),(194,'Matiz',9),(195,'Monte Carlo',9),(196,'Nubira',9),(197,'Orlando',9),(198,'Spark',9),(199,'Suburban',9),(200,'Tacuma',9),(201,'Tahoe',9),(202,'Trax',9),(203,'911 Carrera',10),(204,'911 Carrera Cabrio',10),(205,'911 Targa',10),(206,'911 Turbo',10),(207,'924',10),(208,'944',10),(209,'997',10),(210,'Boxster',10),(211,'Cayenne',10),(212,'Cayman',10),(213,'Macan',10),(214,'Panamera',10),(215,'Accord',11),(216,'Accord Coupé',11),(217,'Accord Tourer',11),(218,'City',11),(219,'Civic',11),(220,'Civic Aerodeck',11),(221,'Civic Coupé',11),(222,'Civic Tourer',11),(223,'Civic Type R',11),(224,'CR-V',11),(225,'CR-X',11),(226,'CR-Z',11),(227,'FR-V',11),(228,'HR-V',11),(229,'Insight',11),(230,'Integra',11),(231,'Jazz',11),(232,'Legend',11),(233,'Prelude',11),(234,'BRZ',12),(235,'Forester',12),(236,'Impreza',12),(237,'Impreza Wagon',12),(238,'Justy',12),(239,'Legacy',12),(240,'Legacy Wagon',12),(241,'Legacy Outback',12),(242,'Levorg',12),(243,'Outback',12),(244,'SVX',12),(245,'Tribeca',12),(246,'Tribeca B9',12),(247,'XV',12),(248,'121',13),(249,'2',13),(250,'3',13),(251,'323',13),(252,'323 Combi',13),(253,'323 Coupé',13),(254,'323 F',13),(255,'5',13),(256,'6',13),(257,'6 Combi',13),(258,'626',13),(259,'626 Combi',13),(260,'B-Fighter',13),(261,'B2500',13),(262,'BT',13),(263,'CX-3',13),(264,'CX-5',13),(265,'CX-7',13),(266,'CX-9',13),(267,'Demio',13),(268,'MPV',13),(269,'MX-3',13),(270,'MX-5',13),(271,'MX-6',13),(272,'Premacy',13),(273,'RX-7',13),(274,'RX-8',13),(275,'Xedox 6',13),(276,'3000 GT',14),(277,'ASX',14),(278,'Carisma',14),(279,'Colt',14),(280,'Colt CC',14),(281,'Eclipse',14),(282,'Fuso canter',14),(283,'Galant',14),(284,'Galant Combi',14),(285,'Grandis',14),(286,'L200',14),(287,'L200 Pick up',14),(288,'L200 Pick up Allrad',14),(289,'L300',14),(290,'Lancer',14),(291,'Lancer Combi',14),(292,'Lancer Evo',14),(293,'Lancer Sportback',14),(294,'Outlander',14),(295,'Pajero',14),(296,'Pajeto Pinin',14),(297,'Pajero Pinin Wagon',14),(298,'Pajero Sport',14),(299,'Pajero Wagon',14),(300,'Space Star',14),(301,'CT',15),(302,'GS',15),(303,'GS 300',15),(304,'GX',15),(305,'Iммм',15),(306,'IS 200',15),(307,'IS 250 C',15),(308,'IS-F',15),(309,'LS',15),(310,'LX',15),(311,'NX',15),(312,'RC F',15),(313,'RX',15),(314,'RX 300',15),(315,'RX 400h',15),(316,'RX 450h',15),(317,'SC 430',15),(318,'4-Runner',16),(319,'Auris',16),(320,'Avensis',16),(321,'Avensis Combi',16),(322,'Avensis Van Verso',16),(323,'Aygo',16),(324,'Camry',16),(325,'Carina',16),(326,'Celica',16),(327,'Corolla',16),(328,'Corolla Combi',16),(329,'Corolla sedan',16),(330,'Corolla Verso',16),(331,'FJ Cruiser',16),(332,'GT86',16),(333,'Hiace',16),(334,'Hiace Van',16),(335,'Highlander',16),(336,'Hilux',16),(337,'Land Cruiser',16),(338,'MR2',16),(339,'Paseo',16),(340,'Picnic',16),(341,'Prius',16),(342,'RAV4',16),(343,'Sequoia',16),(344,'Starlet',16),(345,'Supra',16),(346,'Tundra',16),(347,'Urban Cruiser',16),(348,'Verso',16),(349,'Yaris',16),(350,'Yaris Verso',16),(351,'i3',17),(352,'i8',17),(353,'M3',17),(354,'M4',17),(355,'M5',17),(356,'M6',17),(357,'Rad 1',17),(358,'Rad 1 Cabrio',17),(359,'Rad 1 Coupé',17),(360,'Rad 2',17),(361,'Rad 2 Active Tourer',17),(362,'Rad 2 Coupé',17),(363,'Rad 2 Gran Tourer',17),(364,'Rad 3',17),(365,'Rad 3 Cabrio',17),(366,'Rad 3 Compact',17),(367,'Rad 3 Coupé',17),(368,'Rad 3 GT',17),(369,'Rad 3 Touring',17),(370,'Rad 4',17),(371,'Rad 4 Cabrio',17),(372,'Rad 4 Gran Coupé',17),(373,'Rad 5',17),(374,'Rad 5 GT',17),(375,'Rad 5 Touring',17),(376,'Rad 6',17),(377,'Rad 6 Cabrio',17),(378,'Rad 6 Coupé',17),(379,'Rad 6 Gran Coupé',17),(380,'Rad 7',17),(381,'Rad 8 Coupé',17),(382,'X1',17),(383,'X3',17),(384,'X4',17),(385,'X5',17),(386,'X6',17),(387,'Z3',17),(388,'Z3 Coupé',17),(389,'Z3 Roadster',17),(390,'Z4',17),(391,'Z4 Roadster',17),(392,'Amarok',18),(393,'Beetle',18),(394,'Bora',18),(395,'Bora Variant',18),(396,'Caddy',18),(397,'Caddy Van',18),(398,'Life',18),(399,'California',18),(400,'Caravelle',18),(401,'CC',18),(402,'Crafter',18),(403,'Crafter Van',18),(404,'Crafter Kombi',18),(405,'CrossTouran',18),(406,'Eos',18),(407,'Fox',18),(408,'Golf',18),(409,'Golf Cabrio',18),(410,'Golf Plus',18),(411,'Golf Sportvan',18),(412,'Golf Variant',18),(413,'Jetta',18),(414,'LT',18),(415,'Lupo',18),(416,'Multivan',18),(417,'New Beetle',18),(418,'New Beetle Cabrio',18),(419,'Passat',18),(420,'Passat Alltrack',18),(421,'Passat CC',18),(422,'Passat Variant',18),(423,'Passat Variant Van',18),(424,'Phaeton',18),(425,'Polo',18),(426,'Polo Van',18),(427,'Polo Variant',18),(428,'Scirocco',18),(429,'Sharan',18),(430,'T4',18),(431,'T4 Caravelle',18),(432,'T4 Multivan',18),(433,'T5',18),(434,'T5 Caravelle',18),(435,'T5 Multivan',18),(436,'T5 Transporter Shuttle',18),(437,'Tiguan',18),(438,'Touareg',18),(439,'Touran',18),(440,'Alto',19),(441,'Baleno',19),(442,'Baleno kombi',19),(443,'Grand Vitara',19),(444,'Grand Vitara XL-7',19),(445,'Ignis',19),(446,'Jimny',19),(447,'Kizashi',19),(448,'Liana',19),(449,'Samurai',19),(450,'Splash',19),(451,'Swift',19),(452,'SX4',19),(453,'SX4 Sedan',19),(454,'Vitara',19),(455,'Wagon R+',19),(456,'100 D',20),(457,'115',20),(458,'124',20),(459,'126',20),(460,'190',20),(461,'190 D',20),(462,'190 E',20),(463,'200 - 300',20),(464,'200 D',20),(465,'200 E',20),(466,'210 Van',20),(467,'210 kombi',20),(468,'310 Van',20),(469,'310 kombi',20),(470,'230 - 300 CE Coupé',20),(471,'260 - 560 SE',20),(472,'260 - 560 SEL',20),(473,'500 - 600 SEC Coupé',20),(474,'Trieda A',20),(475,'A',20),(476,'A L',20),(477,'AMG GT',20),(478,'Trieda B',20),(479,'Trieda C',20),(480,'C',20),(481,'C Sportcoupé',20),(482,'C T',20),(483,'Citan',20),(484,'CL',20),(485,'CL',20),(486,'CLA',20),(487,'CLC',20),(488,'CLK Cabrio',20),(489,'CLK Coupé',20),(490,'CLS',20),(491,'Trieda E',20),(492,'E',20),(493,'E Cabrio',20),(494,'E Coupé',20),(495,'E T',20),(496,'Trieda G',20),(497,'G Cabrio',20),(498,'GLvvvvvvvvvvv',20),(499,'GLA',20),(500,'GLCzzzz',20),(501,'GLE',20),(502,'GLK',20),(503,'Trieda M',20),(504,'MB 100',20),(505,'Trieda R',20),(506,'Trieda S',20),(507,'S',20),(508,'S Coupé',20),(509,'SL',20),(510,'SLC',20),(511,'SLK',20),(512,'SLR',20),(513,'Sprinter',20),(514,'9-3',21),(515,'9-3 Cabriolet',21),(516,'9-3 Coupé',21),(517,'9-3 SportCombi',21),(518,'9-5',21),(519,'9-5 SportCombi',21),(520,'900',21),(521,'900 C',21),(522,'900 C Turbo',21),(523,'9000',21),(524,'100',22),(525,'100 Avant',22),(526,'80',22),(527,'80 Avant',22),(528,'80 Cabrio',22),(529,'90',22),(530,'A1',22),(531,'A88',22),(532,'A3',22),(533,'A3 Cabriolet',22),(534,'A3 Limuzina',22),(535,'A3 Sportback',22),(536,'A4',22),(537,'A4 Allroad',22),(538,'A4 Avant',22),(539,'A4 Cabriolet',22),(540,'A5',22),(541,'A5 Cabriolet',22),(542,'A5 Sportback',22),(543,'A6',22),(544,'A6 Allroad',22),(545,'A6 Avant',22),(546,'A7',22),(547,'A99',22),(548,'A8 Long',22),(549,'Q3',22),(550,'Q5',22),(551,'Q7',22),(552,'R8',22),(553,'RS4 Cabriolet',22),(554,'RS4/RS4 Avant',22),(555,'RS5',22),(556,'RS6 Avant',22),(557,'RS7',22),(558,'S3/S3 Sportback',22),(559,'S4 Cabriolet',22),(560,'S4/S4 Avant',22),(561,'S5/S5 Cabriolet',22),(562,'S6/RS6',22),(563,'S7',22),(564,'S8',22),(565,'SQ5',22),(566,'TT Coupé',22),(567,'TT Roadster',22),(568,'TTS',22),(569,'Avella',23),(570,'Besta',23),(571,'Carens',23),(572,'Carnival',23),(573,'Cee`d',23),(574,'Cee`d SW',23),(575,'Cerato',23),(576,'K 2500',23),(577,'Magentis',23),(578,'Opirus',23),(579,'Optima',23),(580,'Picanto',23),(581,'Pregio',23),(582,'Pride',23),(583,'Pro Cee`d',23),(584,'Rio',23),(585,'Rio Combi',23),(586,'Rio sedan',23),(587,'Sephia',23),(588,'Shuma',23),(589,'Sorento',23),(590,'Soul',23),(591,'Sportage',23),(592,'Venga',23),(593,'109',24),(594,'Defender',24),(595,'Discovery',24),(596,'Discovery Sport',24),(597,'Freelander',24),(598,'Range Rover',24),(599,'Range Rover Evoque',24),(600,'Range Rover Sport',24),(601,'Avenger',25),(602,'Caliber',25),(603,'Challenger',25),(604,'Charger',25),(605,'Grand Caravan',25),(606,'Journey',25),(607,'Magnum',25),(608,'Nitro',25),(609,'RAM',25),(610,'Stealth',25),(611,'Viper',25),(612,'300 C',26),(613,'300 C Touring',26),(614,'300 M',26),(615,'Crossfire',26),(616,'Grand Voyager',26),(617,'LHS',26),(618,'Neon',26),(619,'Pacifica',26),(620,'Plymouth',26),(621,'PT Cruiser',26),(622,'Sebring',26),(623,'Sebring Convertible',26),(624,'Stratus',26),(625,'Stratus Cabrio',26),(626,'Town & Country',26),(627,'Voyager',26),(628,'Aerostar',27),(629,'B-Max',27),(630,'C-Max',27),(631,'Cortina',27),(632,'Cougar',27),(633,'Edge',27),(634,'Escort',27),(635,'Escort Cabrio',27),(636,'Escort kombi',27),(637,'Explorer',27),(638,'F-150',27),(639,'F-250',27),(640,'Fiesta',27),(641,'Focus',27),(642,'Focus C-Max',27),(643,'Focus CC',27),(644,'Focus kombi',27),(645,'Fusion',27),(646,'Galaxy',27),(647,'Grand C-Max',27),(648,'Ka',27),(649,'Kuga',27),(650,'Maverick',27),(651,'Mondeo',27),(652,'Mondeo Combi',27),(653,'Mustang',27),(654,'Orion',27),(655,'Puma',27),(656,'Ranger',27),(657,'S-Max',27),(658,'Sierra',27),(659,'Street Ka',27),(660,'Tourneo Connect',27),(661,'Tourneo Custom',27),(662,'Transit',27),(663,'Transit',27),(664,'Transit Bus',27),(665,'Transit Connect LWB',27),(666,'Transit Courier',27),(667,'Transit Custom',27),(668,'Transit kombi',27),(669,'Transit Tourneo',27),(670,'Transit Valnik',27),(671,'Transit Van',27),(672,'Transit Van 350',27),(673,'Windstar',27),(674,'H2',28),(675,'H3',28),(676,'Accent',29),(677,'Atos',29),(678,'Atos Prime',29),(679,'Coupé',29),(680,'Elantra',29),(681,'Galloper',29),(682,'Genesis',29),(683,'Getzzz',29),(684,'Grandeur',29),(685,'H 350',29),(686,'H1',29),(687,'H1 Bus',29),(688,'H1 Van',29),(689,'H200',29),(690,'i10',29),(691,'i20',29),(692,'i30',29),(693,'i30 CW',29),(694,'i40',29),(695,'i40 CW',29),(696,'ix20',29),(697,'ix35',29),(698,'ix55',29),(699,'Lantra',29),(700,'Matrix',29),(701,'Santa Fe',29),(702,'Sonata',29),(703,'Terracan',29),(704,'Trajet',29),(705,'Tucson',29),(706,'Veloster',29),(707,'EX',30),(708,'FX',30),(709,'G',30),(710,'G Coupé',30),(711,'M',30),(712,'Q',30),(713,'QX',30),(714,'Daimler',31),(715,'F-Pace',31),(716,'F-Type',31),(717,'S-Type',31),(718,'Sovereign',31),(719,'X-Type',31),(720,'X-type Estate',31),(721,'XE',31),(722,'XF',31),(723,'XJ',31),(724,'XJ12',31),(725,'XJ6',31),(726,'XJ8',31),(727,'XJ8',31),(728,'XJR',31),(729,'XK',31),(730,'XK8 Convertible',31),(731,'XKR',31),(732,'XKR Convertible',31),(733,'Cherokee',32),(734,'Commander',32),(735,'Compass',32),(736,'Grand Cherokee',32),(737,'Patriot',32),(738,'Renegade',32),(739,'Wrangler',32),(740,'100 NX',33),(741,'200 SX',33),(742,'350 Z',33),(743,'350 Z Roadster',33),(744,'370 Z',33),(745,'Almera',33),(746,'Almera Tino',33),(747,'Cabstar E - T',33),(748,'Cabstar TL2 Valnik',33),(749,'e-NV200',33),(750,'GT-R',33),(751,'Insterstar',33),(752,'Juke',33),(753,'King Cab',33),(754,'Leaf',33),(755,'Maxima',33),(756,'Maxima QX',33),(757,'Micra',33),(758,'Murano',33),(759,'Navara',33),(760,'Note',33),(761,'NP300 Pickup',33),(762,'NV200',33),(763,'NV400',33),(764,'Pathfinder',33),(765,'Patrol',33),(766,'Patrol GR',33),(767,'Pickup',33),(768,'Pixo',33),(769,'Primastar',33),(770,'Primastar Combi',33),(771,'Primera',33),(772,'Primera Combi',33),(773,'Pulsar',33),(774,'Qashqai',33),(775,'Serena',33),(776,'Sunny',33),(777,'Terrano',33),(778,'Tiida',33),(779,'Trade',33),(780,'Vanette Cargo',33),(781,'X-Trail',33),(782,'240',34),(783,'340',34),(784,'360',34),(785,'460',34),(786,'850',34),(787,'850 kombi',34),(788,'C30',34),(789,'C70',34),(790,'C70 Cabrio',34),(791,'C70 Coupé',34),(792,'S40',34),(793,'S60',34),(794,'S70',34),(795,'S80',34),(796,'S90',34),(797,'V40',34),(798,'V50',34),(799,'V60',34),(800,'V70',34),(801,'V90',34),(802,'XC60',34),(803,'XC70',34),(804,'XC90',34),(805,'Espero',35),(806,'Kalos',35),(807,'Lacetti',35),(808,'Lanos',35),(809,'Leganza',35),(810,'Lublin',35),(811,'Matiz',35),(812,'Nexia',35),(813,'Nubira',35),(814,'Nubira kombi',35),(815,'Racer',35),(816,'Tacuma',35),(817,'Tico',35),(818,'1100',36),(819,'126',36),(820,'500',36),(821,'500L',36),(822,'500X',36),(823,'850',36),(824,'Barchetta',36),(825,'Brava',36),(826,'Cinquecento',36),(827,'Coupé',36),(828,'Croma',36),(829,'Doblo',36),(830,'Doblo Cargo',36),(831,'Doblo Cargo Combi',36),(832,'Ducato',36),(833,'Ducato Van',36),(834,'Ducato Kombi',36),(835,'Ducato Podvozok',36),(836,'Florino',36),(837,'Florino Combi',36),(838,'Freemont',36),(839,'Grande Punto',36),(840,'Idea',36),(841,'Linea',36),(842,'Marea',36),(843,'Marea Weekend',36),(844,'Multipla',36),(845,'Palio Weekend',36),(846,'Panda',36),(847,'Panda Van',36),(848,'Punto',36),(849,'Punto Cabriolet',36),(850,'Punto Evo',36),(851,'Punto Van',36),(852,'Qubo',36),(853,'Scudo',36),(854,'Scudo Van',36),(855,'Scudo Kombi',36),(856,'Sedici',36),(857,'Seicento',36),(858,'Stilo',36),(859,'Stilo Multiwagon',36),(860,'Strada',36),(861,'Talento',36),(862,'Tipo',36),(863,'Ulysse',36),(864,'Uno',36),(865,'X1/9',36),(866,'Cooper',37),(867,'Cooper Cabrio',37),(868,'Cooper Clubman',37),(869,'Cooper D',37),(870,'Cooper D Clubman',37),(871,'Cooper S',37),(872,'Cooper S Cabrio',37),(873,'Cooper S Clubman',37),(874,'Countryman',37),(875,'Mini One',37),(876,'One D',37),(877,'200',38),(878,'214',38),(879,'218',38),(880,'25',38),(881,'400',38),(882,'414',38),(883,'416',38),(884,'620',38),(885,'75',38),(886,'Cabrio',39),(887,'City-Coupé',39),(888,'Compact Pulse',39),(889,'Forfour',39),(890,'Fortwo cabrio',39),(891,'Fortwo coupé',39),(892,'Roadster',39),(893,'A2222',22),(894,'99',22),(895,'A8',22),(896,'A999',22),(897,'Getz',29),(898,'GLSxxxx',20),(899,'GLCxxxxx',20),(900,'Getz',15),(901,'ISss',15),(902,'ISSSSS',15),(903,'ISzz',15),(904,'GL',20),(905,'GLccc',20),(906,'GLSsssss',20),(907,'Getzzzzz',29),(908,'GLC',20),(909,'GLSzzzzzzz',20),(910,'GLSzz',20),(911,'Getzxxxxx',29),(912,'Getzzzzzz',29),(913,'A9',22),(914,'GLC',20),(915,'GLjj',20),(916,'GLC',20),(917,'GLS',20),(918,'IS',15),(919,'GLCzzzzz',20),(920,'GLC',20),(921,'Get',29),(922,'Getz',29),(923,'GLC',20),(924,'A2',22),(925,'GK',20),(926,'GL',20),(927,'GL',20),(928,'GLvbvv',20),(929,'GLvbvvghgg',20),(930,'GL',20),(931,'GL',20),(932,'GL',20),(933,'Getz',29),(934,'GL',20),(935,'GL',20),(936,'Getz',29),(937,'GL',20),(938,'Getz',29),(939,'GL',20),(940,'A10',22),(941,'A0',22),(942,'99',18),(943,'G23',15),(944,'66',15),(945,'99',18),(946,'F222',15),(947,'90',17),(948,'88',19),(949,'89',17),(950,'56',17),(951,'88',16),(952,'A88',22),(953,'G5',21),(954,'F2',15),(955,'GK',20),(956,'G5',18),(957,'G8',13),(958,'P56',17),(959,'P56',17),(960,'G1',16),(961,'F2',15),(962,'88',11),(963,'25',9),(964,'12',16),(965,'A55',4),(966,'A5',10);
/*!40000 ALTER TABLE `models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_date` date NOT NULL DEFAULT current_timestamp(),
  `due_date` date NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `status` enum('new','processing','done') DEFAULT 'new',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_orders_vehicle1_idx` (`vehicle_id`),
  KEY `fk_orders_users1_idx` (`users_id`),
  CONSTRAINT `fk_orders_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_vehicle1` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (98,'2020-01-22','2021-02-17',8,4,'N.A.','done',0),(99,'2021-06-10','2021-06-12',33,4,'N.A','done',0),(100,'2021-05-03','2021-06-30',27,4,'ASAP','processing',0),(101,'2021-05-11','2021-06-22',13,4,'call when its done','new',0),(102,'2021-03-08','2021-03-30',17,4,'N.A','done',0),(103,'2020-12-15','2020-12-28',14,4,'it is possible to take more time','done',0),(104,'2021-06-02','2021-06-27',24,4,'N.A.','processing',0),(105,'2020-10-06','2020-12-08',26,4,'some additional checks required','done',0),(106,'2021-06-07','2021-06-23',19,4,'N.A','new',0),(107,'2021-06-08','2021-06-22',8,4,'ASAP','new',0),(108,'2021-06-09','2021-08-09',33,4,'ASAP','done',0),(109,'2021-06-16','2021-06-23',30,4,'might need more time','processing',0),(110,'2021-06-07','2021-06-13',13,16,'N.A','processing',0),(111,'2021-06-01','2021-06-25',20,16,'as soon as possible','processing',0),(112,'2021-06-10','2021-06-30',33,16,'not urgent','new',0),(113,'2021-04-05','2021-05-01',22,16,'N.A','done',0),(114,'2020-01-15','2021-02-17',19,16,'ASAP','done',0),(115,'2020-06-23','2020-08-18',6,16,'a lot of services required','done',0),(116,'2020-07-09','2020-09-23',25,16,'need to be checked carefully','done',0),(117,'2020-09-09','2020-10-30',30,16,'ASAP','done',0),(118,'2020-11-01','2020-12-02',14,16,'call to customer when its ready','done',0),(119,'2021-04-15','2021-04-28',17,16,'ASAP','done',0),(120,'2021-06-10','2021-06-30',26,16,'call to customer','processing',0),(121,'2021-06-01','2021-06-29',19,16,'N.A','processing',0),(122,'2020-05-24','2020-05-31',22,16,'N.A','done',0),(123,'2020-02-09','2020-02-15',13,16,'ASAP','done',0),(124,'2020-06-11','2020-06-16',19,16,'check carefully','done',0),(125,'2020-02-21','2020-06-29',25,16,'urgent','done',0),(126,'2020-02-12','2020-02-16',19,16,'ASAP','done',0);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_has_services`
--

DROP TABLE IF EXISTS `orders_has_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_has_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `services_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_has_services_orders1_idx` (`orders_id`),
  KEY `fk_orders_has_services_services1_idx` (`services_id`),
  CONSTRAINT `fk_orders_has_services_orders1` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_has_services_services1` FOREIGN KEY (`services_id`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=324 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_has_services`
--

LOCK TABLES `orders_has_services` WRITE;
/*!40000 ALTER TABLE `orders_has_services` DISABLE KEYS */;
INSERT INTO `orders_has_services` VALUES (227,98,3),(228,98,1),(229,98,32),(230,98,4),(231,99,49),(232,99,42),(233,99,47),(234,100,31),(235,100,37),(236,100,2),(237,101,40),(238,101,50),(239,101,39),(240,102,33),(241,102,38),(242,102,49),(243,103,46),(244,103,34),(245,103,47),(246,104,50),(247,104,43),(248,104,31),(249,105,42),(250,105,47),(251,106,3),(252,106,1),(253,106,32),(254,107,49),(255,107,42),(256,107,47),(257,108,44),(258,108,38),(259,108,33),(260,109,42),(261,109,41),(262,110,43),(263,110,31),(264,110,37),(265,111,5),(266,111,36),(267,111,48),(268,111,47),(269,112,35),(270,112,46),(271,113,46),(272,113,41),(273,113,39),(274,114,2),(275,114,45),(276,114,44),(277,114,33),(278,114,38),(279,115,49),(280,115,42),(281,115,33),(282,115,38),(283,115,44),(284,115,45),(285,115,2),(286,115,37),(287,115,31),(288,115,43),(289,116,37),(290,116,43),(291,116,36),(292,116,4),(293,116,42),(294,117,40),(295,117,50),(296,117,39),(297,117,41),(298,117,46),(299,118,41),(300,118,32),(301,118,42),(302,118,37),(303,119,46),(304,119,40),(305,120,33),(306,120,42),(307,120,34),(308,121,31),(309,122,41),(310,122,46),(311,122,34),(312,123,37),(313,123,48),(314,123,46),(315,124,38),(316,124,49),(317,124,33),(318,124,42),(319,124,47),(320,125,38),(321,125,5),(322,125,35),(323,126,39);
/*!40000 ALTER TABLE `orders_has_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset_passwords`
--

DROP TABLE IF EXISTS `reset_passwords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reset_passwords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_time` datetime NOT NULL DEFAULT current_timestamp(),
  `expire_time` datetime NOT NULL DEFAULT (current_timestamp() + interval 1 hour),
  `user_reset_id` int(11) DEFAULT NULL,
  `token` varchar(500) DEFAULT NULL,
  `is_used` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK1_reset_passwords_users1` (`user_reset_id`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset_passwords`
--

LOCK TABLES `reset_passwords` WRITE;
/*!40000 ALTER TABLE `reset_passwords` DISABLE KEYS */;
INSERT INTO `reset_passwords` VALUES (1,'2021-05-26 10:22:46','2021-05-26 11:22:46',8,'00e18498-7445-44cb-a34e-8d56250d1650',0),(2,'2021-05-26 10:28:56','2021-05-26 11:28:56',8,'77a9e64b-80fc-48f8-85ba-54634e617ccf',0),(3,'2021-05-26 10:32:41','2021-05-26 11:32:41',8,'e64ab08d-fd38-4e99-92ed-3b030f7e6dc6',0),(4,'2021-05-26 10:33:22','2021-05-26 11:33:22',8,'113699d7-339e-47c5-9887-85b69cee4d97',0),(5,'2021-05-26 10:35:22','2021-05-26 11:35:22',8,'9deb33f3-9304-47e7-bb9f-be111f9f9f2d',0),(6,'2021-05-26 10:40:15','2021-05-26 11:40:15',8,'e1ce9921-3536-4d4f-98b8-05e24209f387',0),(7,'2021-05-26 11:26:57','2021-05-26 12:26:57',9,'993408a5-acbd-47cb-ba04-2341504dd5f0',0),(8,'2021-05-26 15:16:16','2021-05-26 16:16:16',9,'37c17438-4ff5-4cb2-9b3f-b04a8a321490',0),(9,'2021-05-26 16:17:35','2021-05-26 17:17:35',11,'06e52758-4868-4dd9-a028-cc9b0b8f9391',0),(10,'2021-05-26 16:19:29','2021-05-26 17:19:29',11,'e350ff1d-4bf1-4783-b0b5-a862f174a38f',0),(11,'2021-05-26 16:29:40','2021-05-26 17:29:40',11,'0717b887-97ea-47c3-940b-4d7e2837fd96',0),(12,'2021-05-26 17:33:17','2021-05-26 18:33:17',11,'c681311a-b8c9-40e1-a473-33eba507eaea',1),(13,'2021-05-26 17:34:01','2021-05-26 18:34:01',11,'cf60c8af-aa3d-4579-bf81-f765e35597fb',0),(14,'2021-05-26 17:55:21','2021-05-26 18:55:21',11,'d8d18891-6666-4553-8455-4c7565956391',0),(15,'2021-05-26 18:00:44','2021-05-26 19:00:44',11,'ac8866bc-115a-4a94-8970-f86b5e27aa37',0),(16,'2021-05-26 18:02:19','2021-05-26 19:02:19',2,'eaf8bbaa-c31f-448f-8550-e2c6b945ee04',1),(17,'2021-05-26 18:11:23','2021-05-26 19:11:23',2,'7799f306-42a5-4171-b876-105601568ec7',1),(18,'2021-05-27 21:01:27','2021-05-27 22:01:27',2,'64bf45cb-2ebf-4a47-9f45-a502707563e3',0),(19,'2021-05-27 21:14:45','2021-05-27 22:14:45',2,'d5d05b07-d67c-4dcd-812e-300e05bbb91d',1),(20,'2021-05-28 21:35:17','2021-05-28 22:35:17',5,'31e3ab7f-68f8-4534-b18f-aa0ab50bd3f6',0),(21,'2021-05-28 21:42:05','2021-05-28 22:42:05',1,'dbf21815-a5ca-4a7b-9b05-d125ca2f1e2f',0),(22,'2021-05-28 21:57:45','2021-05-28 22:57:45',5,'d7a13fb1-df4c-4b8b-9830-4d687fa8454e',0),(23,'2021-05-28 21:59:09','2021-05-28 22:59:09',5,'cfb6fcd8-92b2-4792-910e-9a8f16d77f48',0),(24,'2021-05-28 22:00:09','2021-05-28 23:00:09',5,'39db41a6-1f73-43cf-a5bc-f16fc8adf8d0',0),(25,'2021-05-28 22:02:38','2021-05-28 23:02:38',5,'255cfe99-9ce0-440d-9d82-61f3c757866b',0),(26,'2021-05-28 22:05:04','2021-05-28 23:05:04',5,'fa8cffb9-a030-4bc6-9009-20498634047f',0),(27,'2021-05-28 22:05:24','2021-05-28 23:05:24',5,'9c2472b2-6cd2-431b-9328-7bf98c37881f',0),(28,'2021-05-28 22:06:04','2021-05-28 23:06:04',5,'d74f6cff-e8dd-4ef2-9619-56a1229f49c6',0),(29,'2021-05-28 22:06:51','2021-05-28 23:06:51',5,'55a6a931-7001-4761-8d3a-abbbc6f027bf',0),(30,'2021-05-28 22:24:40','2021-05-28 23:24:40',1,'e3bae85a-0de2-4b14-a07f-90c0f4224925',0),(31,'2021-05-28 22:26:50','2021-05-28 23:26:50',1,'3254f6d5-3a92-4cd7-8e7c-4a6666da2624',0),(32,'2021-05-28 23:50:48','2021-05-29 00:50:48',1,'410fa664-be61-44f2-9736-44ff6c8d8c94',0),(33,'2021-05-29 00:28:29','2021-05-29 01:28:29',1,'3a238f29-9f6f-4af5-9e12-6fe6ff3e03dd',0),(34,'2021-05-30 17:41:56','2021-05-30 18:41:56',4,'1dc17d51-3659-4346-986f-acf6095ec22d',0),(35,'2021-05-30 19:56:01','2021-05-30 20:56:01',4,'e6a637b1-68a7-4590-9fd8-95876d752df9',0),(36,'2021-05-30 20:13:35','2021-05-30 21:13:35',4,'b9637246-d5a5-47ad-9097-e11514ba4530',0),(37,'2021-05-30 20:27:05','2021-05-30 21:27:05',4,'3cb681ee-a47d-4b28-bd71-c21949a8f52a',0),(38,'2021-05-30 20:30:35','2021-05-30 21:30:35',4,'e6ee992c-5789-402a-a700-f3d3e84b158e',0),(39,'2021-05-30 20:34:03','2021-05-30 21:34:03',4,'46e3e825-e6a6-47f6-9da3-b6860ea1a417',0),(40,'2021-05-30 20:35:07','2021-05-30 21:35:07',4,'dbd7c785-59db-4b0f-b214-c33096444b13',0),(41,'2021-05-30 20:35:53','2021-05-30 21:35:53',4,'a6ef7e73-a338-40f8-8420-618608de8866',0),(42,'2021-05-30 20:47:35','2021-05-30 21:47:35',4,'8e622e55-4e9f-48a2-a1f2-bc5422b620c1',0),(43,'2021-05-30 20:53:26','2021-05-30 21:53:26',4,'91adf6e6-f2f4-471a-b517-fc5cb2d97041',0),(44,'2021-05-30 20:55:26','2021-05-30 21:55:26',4,'e4c7d64f-538b-4550-b9cc-60c37d608dbc',0),(45,'2021-05-30 20:56:57','2021-05-30 21:56:57',4,'601af26e-b15e-47fc-b87f-81bb3dba0b81',0),(46,'2021-05-30 21:04:25','2021-05-30 22:04:25',4,'b061dacd-b83c-4976-beea-9e8f9195c319',0),(47,'2021-05-30 21:07:22','2021-05-30 22:07:22',4,'8cae83d3-43be-4362-a1a3-c476dd3cf479',0),(48,'2021-05-30 21:09:06','2021-05-30 22:09:06',4,'a8aa709c-b0a4-4a30-8ec1-fe3332b9e8ee',0),(49,'2021-05-30 21:10:59','2021-05-30 22:10:59',4,'6a9a832a-fe6a-4b99-b395-202828b37832',0),(50,'2021-05-30 21:16:52','2021-05-30 22:16:52',4,'a2d4d47d-61a6-47d1-a9e4-2edcd91c1924',1),(51,'2021-05-30 21:18:42','2021-05-30 22:18:42',4,'c1c787c3-a066-4140-9cee-4e4a70c27fdc',1),(52,'2021-05-31 01:34:11','2021-05-31 02:34:11',4,'9988854f-fd56-49e0-a2c0-02792ab072c9',1),(53,'2021-05-31 01:36:34','2021-05-31 02:36:34',4,'ab67f9ef-a27d-40c7-98b1-dc9a8d237471',1),(54,'2021-05-31 01:39:57','2021-05-31 02:39:57',4,'fcacd9ad-6eea-42a3-94f5-dada10460c51',1),(55,'2021-05-31 01:42:22','2021-05-31 02:42:22',4,'3089fd4a-8c77-45c1-9674-43db2c9b788e',1),(56,'2021-05-31 08:15:04','2021-05-31 09:15:04',4,'2f0f6242-2c51-40f8-8177-5641fdd8cc6c',1),(57,'2021-05-31 08:29:16','2021-05-31 09:29:16',4,'97055061-0624-47e3-8eae-c50088321f08',0),(58,'2021-05-31 08:30:49','2021-05-31 09:30:49',1,'29ae015b-57e7-4610-9b00-270653fcab30',0),(59,'2021-05-31 08:34:37','2021-05-31 09:34:37',4,'93dbc981-11f0-4721-982f-3cc25d01634f',0),(60,'2021-05-31 16:12:48','2021-05-31 17:12:48',4,'f39bb2d6-e78e-44e7-bb09-171bb4fcffa9',1),(61,'2021-06-02 20:50:16','2021-06-02 21:50:16',16,'2d037f38-6221-47be-8916-f1442e10221d',1),(62,'2021-06-08 20:57:42','2021-06-08 21:57:42',4,'4726ad96-6ad3-4ff0-bee9-0585155f3f2d',0),(63,'2021-06-08 21:30:18','2021-06-08 22:30:18',4,'43eb734c-168c-475f-a101-8d57bde894e4',0),(64,'2021-06-08 21:32:01','2021-06-08 22:32:01',4,'390448c9-ae5d-498f-9519-cc3cdf3ada2c',0),(65,'2021-06-08 22:18:13','2021-06-08 23:18:13',4,'72548f6b-3c92-45d8-a984-ab0592d57d09',0),(66,'2021-06-08 22:20:32','2021-06-08 23:20:32',4,'02127084-5f51-4c8b-b9f7-c332dbef0fef',0),(67,'2021-06-08 22:21:08','2021-06-08 23:21:08',4,'a94c4d49-8f30-420f-b05a-b36703109499',0),(68,'2021-06-08 23:34:17','2021-06-09 00:34:17',4,'3f3c279b-c495-45e6-9847-e406ada511d1',0),(69,'2021-06-09 17:29:56','2021-06-09 18:29:56',16,'a9c28d6f-2ac4-4357-9f7c-b6cd59be99e0',1);
/*!40000 ALTER TABLE `reset_passwords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `price` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Oil change\r\n\r\n',15,0),(2,'Change Brake Fluide',58,0),(3,'Power Steering',10,0),(4,'Check engine light',20,0),(5,'Diesel Oil Change',25,0),(31,'Tyre change',50,0),(32,'Diagnostics',20,0),(33,'Air conditioner repair',70,0),(34,'Suspension maintenance',100,0),(35,'Headlights replacement',120,0),(36,'Throttle cable repair',30,0),(37,'Alternator repair',50,0),(38,'Airbag maintenance',60,0),(39,'Sunroof maintenance',150,0),(40,'Headgasket change',300,0),(41,'Heater maintenance',130,0),(42,'Fuel system maintenance',80,0),(43,'Mirror sensors replacement',40,0),(44,'Water pump replacement',60,0),(45,'Radiator replacement',60,0),(46,'A/C recharge',120,0),(47,'Exhaust welding',90,0),(48,'Hydraulic pump change',40,0),(49,'Bearing replacement',75,0),(50,'Rims polishment',180,0);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(10) NOT NULL,
  `password` varchar(500) NOT NULL,
  `addresses_id` int(11) NOT NULL,
  `role` enum('employee','customer') NOT NULL DEFAULT 'customer',
  `avatar` varchar(500) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_addresses_idx` (`addresses_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Velina','Koleva','vkoleva@gmail.com','0885654856','$2b$10$sJOtjAozMdDRVmwS9JKuq.svGVpR2lFprYxRA9LOl5g.J5lv0CExW',2,'customer','http://localhost:5555/avatars/1621614782772.jpg',0),(4,'Eva','Stoycheva','evastoycheva@gmail.com','0888143234','$2b$10$G.3U0BE9rA/wn.yBiLxM7.80CRShNuJQGCtNzntHmMiPXIN.4a/7i',3,'employee','http://localhost:5555/avatars/1621614782772.jpg',0),(5,'Anzhelo','Dimitrov','anzhi@gmail.com','0987656342','$2b$10$fOzxgZ2G4OZ5.TRSjmScsOuLJHC8VPLzo3wZxKx.MED0sG66HYA.G',5,'customer','http://localhost:5555/avatars/1621770228340.jpg',0),(12,'Deanna','Dimova','deannadm@abv.bg','0884560189','$2b$10$shOcjzCWp7wS8ybe.VLL8.sMkwuqA/vJidng9g3r/QkJT5cyt2JQK',16,'customer','http://localhost:5555/avatars/1621614782772.jpg',0),(13,'Mariya','Ilieva','m.ilieva@abv.bg','0885654856','$2b$10$G.3U0BE9rA/wn.yBiLxM7.80CRShNuJQGCtNzntHmMiPXIN.4a/7i',1,'customer','http://localhost:5555/avatars/1621614782772.jpg',0),(14,'Georgi','Bachev','georgi@gmail.com','0984545457','$2b$10$GqnVGMJuae2PWIlAd9LlO.lER5mxVORZhR9T37caqgCQ2cXEm541u',17,'customer','http://localhost:5555/avatars/1621770228340.jpg',0),(15,'Boris','Bachev','boris@gmail.com','0786565657','$2b$10$YJNfgSn7eZkWOzf9cKpoOeo4UYeCLYXfJEGyYaIFRvHpwTAmC46I.',18,'customer','http://localhost:5555/avatars/1621770228340.jpg',0),(16,'Deshka','Ilieva','deshka_113@abv.bg','0884573456','$2b$10$e.4mpOhGPEnh0UsIxXCweOAIHC4C2cO/hTsi9y4F3L5/JTEU4fvW6',19,'employee','http://localhost:5555/avatars/1621614782772.jpg',0),(17,'Teodora','Bacheva','tbacheva@gmail.com','0985645341','$2b$10$ZkSv.RvA0Yv0seqzXmJot.qpdrrK/cRfH4dJYmaZAs1Mg3iW1smjW',20,'customer','http://localhost:5555/avatars/1621614782772.jpg',0),(18,'Vili','Bacheva','stoycheva.eva@gmail.com','0987654545','$2b$10$otaI1Z9ZDFjZ69FvlEuD9.nvFEcY4tAI8jAsD8H.HxGX0//NJQCvS',21,'customer','http://localhost:5555/avatars/1621614782772.jpg',0),(19,'Petyr','Todorov','petar@gmail.com','0885768545','$2b$10$dt98P8mmaAJZ6/PocjIHsOllyiH08brUyMA7wJIx13JZBVc/I3SDO',22,'customer','http://localhost:5555/avatars/1621770228340.jpg',0),(20,'Pencho','Georgiev','pencho54@gmail.com','0897676767','$2b$10$rYu4/phJ0grJS7jHYxZ7hOeiLZXDrwlCGvZBPYYCxdxpOqlTXhJUW',23,'customer','http://localhost:5555/avatars/1621770228340.jpg',0),(22,'Pavel','Simov','pavel76@gmail.com','0989898980','$2b$10$zs/oXjhSNaafvFtT.YLO3.vr6T6RyiG02uW0ytOSyb1HoVoyFhAwC',25,'customer','http://localhost:5555/avatars/1621770228340.jpg',0),(24,'Danka','Stoycheva','danka@yahoo.com','0987656342','$2b$10$GfaatgYqQoN0z10CxG6.x.RtJRrUk6564vRpYmSG9ISwKEV4K/0zO',27,'customer','http://localhost:5555/avatars/1621770228340.jpg',0),(25,'Yordanka','Simeonova','yordanka@yahoo.com','0989898988','$2b$10$CfYm0yxHFpNDvAPngka11egVKsgDyfVNVrAidNGWmfvUXH8Y1dvM.',28,'customer','http://localhost:5555/avatars/1621770228340.jpg',0),(26,'Dani','Mitov','daniel@gmail.com','0878343434','$2b$10$Qbu2OEKxMbTHsLf5ag1J6ucYbtSLV6c/NLqWyTlJbvfcaL6fLFkMG',29,'customer','http://localhost:5555/avatars/1621770228340.jpg',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_plate` varchar(45) NOT NULL,
  `year` year(4) NOT NULL,
  `class` enum('A','B','C','D','E','F','S') NOT NULL,
  `vin` varchar(17) NOT NULL,
  `models_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `transmissions` enum('manual','auto') NOT NULL,
  `engine` enum('petrol','diesel','hybrid') NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vin_UNIQUE` (`vin`),
  KEY `fk_vehicle_models1_idx` (`models_id`),
  KEY `FK_vehicle_users` (`customer_id`),
  CONSTRAINT `FK_vehicle_users` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_vehicle_models1` FOREIGN KEY (`models_id`) REFERENCES `models` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicles`
--

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` VALUES (6,'E1255BX',2010,'A','KMHBT51GP4U192200',922,2,'manual','petrol',0),(7,'E1111BX',2010,'A','KMHBT51GP4U194568',922,4,'manual','petrol',0),(8,'E1407BX',2008,'C','KMHBH89GP4U592208',955,5,'manual','petrol',0),(13,'E0607BX',2008,'C','KMHBH89GP4U592266',904,12,'manual','petrol',0),(14,'E6666BX',2008,'C','KMHBH89GP4U592277',925,13,'manual','petrol',0),(17,'CO2232BM',2021,'B','KMHBT51GP4U192255',952,14,'auto','petrol',0),(18,'A0000AA',2020,'A','KMHBH89GP4U596666',945,16,'auto','diesel',0),(19,'A3333BB',2018,'A','KMHBH89GP4U597777',943,17,'auto','hybrid',0),(20,'P1111PA',2020,'A','KMHBH89GP4U598888',944,15,'auto','petrol',0),(22,'А9999BX',2020,'A','KMHBH89GP4U591221',954,18,'auto','hybrid',0),(24,'A1234NB',2020,'A','KMHBH89GP4U596669',954,19,'auto','hybrid',0),(25,'A5478BH',2020,'A','KMHBH89GP4U596660',953,20,'auto','hybrid',0),(26,'A2356',2020,'A','KMHBH89GP4U599999',956,22,'auto','hybrid',0),(27,'С5555НХ',2020,'A','KMHBH89GP4U597878',957,24,'auto','hybrid',0),(30,'С4444НХ',2020,'B','KMHBH89GP4U596767',957,25,'auto','hybrid',0),(33,'E4545HK',2020,'A','KMHBH89GP4U599998',959,26,'auto','hybrid',0),(34,'CA4545BH',2020,'A','KMHBH89GP4U596869',960,18,'auto','hybrid',1),(35,'PA4455BH',2020,'A','KMHBH89GP4U596123',962,15,'auto','hybrid',1),(36,'P8989BH',2020,'A','KMHBH89GP4U596124',963,14,'auto','hybrid',1),(37,'Е3333HX',2020,'A','KMHBH89GP4U596125',964,26,'auto','hybrid',1),(38,'P5656HX',2020,'A','KMHBH89GP4U596126',71,22,'auto','hybrid',1),(39,'E4477BH',2020,'A','KMHBH89GP4U596127',965,25,'auto','hybrid',1),(40,'E6677BH',2020,'A','KMHBH89GP4U596129',966,22,'auto','hybrid',1);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-09 18:47:58
