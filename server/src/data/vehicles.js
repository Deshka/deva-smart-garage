import { queryFuncSql, checkLengthSql } from '../common/helpers.js';
import pool from './pool.js';

const getAll = async (data) => {
  const direction = data.sort || 'ASC';
  const resultsPerPage = data.limit || 20;
  const startIndex = data.page ? (data.page - 1) * data.limit : 0;

  const sql = `
    SELECT v.id,
    m.model_name as model,
    b.brand_name as brand,
    v.year,
    v.reg_plate,
    v.class,
    v.vin,
    v.transmissions,
    v.engine,
    u.first_name,
    u.last_name,
    concat(u.first_name, ' ' , u.last_name) as full_name,
    u.email,
    a.street_address,
    a.city,
    a.country,
    a.postal_code,
    v.is_deleted,
    v.customer_id,
    v.models_id,
    o.issue_date,
    o.due_date
    
    FROM vehicles v
    LEFT JOIN models m ON  m.id =v.models_id
    LEFT JOIN brands b ON  b.id= m.brand_id
    LEFT JOIN users u ON u.id = v.customer_id
    LEFT JOIN addresses a ON a.id = u.addresses_id
    LEFT JOIN orders o ON o.vehicle_id = v.id

    WHERE v.is_deleted = 0
    GROUP by v.id
    ORDER BY v.reg_plate ${direction}
    LIMIT ${startIndex}, ${resultsPerPage}

  `;

  return await pool.query(sql);
};

const searchByQueries = async (
    userQueries,
    vehicleQueries,
    brandQueries,
    modelQueries,
    orderQueries,
    data
) => {
  const direction = data.sort || 'ASC';
  const resultsPerPage = data.limit || 50;
  const startIndex = data.page ? (data.page - 1) * data.limit : 0;

  const sql1 = `
    SELECT v.id,
    m.model_name as model,
    b.brand_name as brand,
    v.year,
    v.reg_plate,
    v.class,
    v.vin,
    v.transmissions,
    v.engine,
    u.first_name, 
    u.last_name,
    u.email,
    u.phone_number,
    a.street_address,
    a.city,
    a.country,
    a.postal_code,
    v.is_deleted,
    v.customer_id,
    v.models_id,
    o.issue_date,
    o.due_date
    
    FROM vehicles v
    JOIN models m ON  m.id = v.models_id
    JOIN brands b ON  b.id= m.brand_id
    JOIN users u ON u.id = v.customer_id
    JOIN addresses a ON a.id = u.addresses_id 
    JOIN orders o ON o.vehicle_id = v.id

    WHERE 
  `;

  const sql2Arr = queryFuncSql(userQueries, 'u');
  const sql2 = checkLengthSql(sql2Arr);

  const sql3Arr = queryFuncSql(vehicleQueries, 'v');
  const sql3 = checkLengthSql(sql3Arr);

  const sql4Arr = queryFuncSql(brandQueries, 'b');
  const sql4 = checkLengthSql(sql4Arr);

  const sql5Arr = queryFuncSql(modelQueries, 'm');
  const sql5 = checkLengthSql(sql5Arr);

  const sql6Arr = queryFuncSql(orderQueries, 'o');
  const sql6 = checkLengthSql(sql6Arr);

  const sqlQueries = sql1.concat(sql2, sql3, sql4, sql5, sql6);
  const sql7 = ` v.is_deleted = 0 `;

  const sql8 = `ORDER BY u.first_name ${direction}
  LIMIT ${startIndex}, ${resultsPerPage}`;

  const sql = sqlQueries + sql7 + sql8;

  return await pool.query(sql);
};

const searchBy = async (column, search, user) => {
  const sql = `
    SELECT v.id,
    m.model_name as model,
    b.brand_name as brand,
    v.year,
    v.reg_plate,
    v.class,
    v.vin,
    v.transmissions,
    v.engine,
    concat(u.first_name, ' ' , u.last_name) as full_name,
    u.email,
    u.phone_number,
    a.street_address,
    a.city,
    a.country,
    a.postal_code,
    v.is_deleted,
    v.customer_id,
    v.models_id,
    o.issue_date,
    o.due_date
    
    FROM vehicles v
    JOIN models m ON  m.id = v.models_id
    JOIN brands b ON  b.id= m.brand_id
    JOIN users u ON u.id = v.customer_id
    JOIN addresses a ON a.id = u.addresses_id
    JOIN orders o ON o.vehicle_id = v.id

    WHERE ${column} LIKE '%${pool
    .escape(search)
    .replace(/'/g, '')}%' and v.is_deleted = 0
  `;

  return await pool.query(sql);
};

const getBy = async (column, value) => {
  const sql = `
  SELECT v.id,
  m.model_name as model,
  b.brand_name as brand,
  v.year,
  v.reg_plate,
  v.class,
  v.vin,
  v.transmissions,
  v.engine,
  concat(u.first_name, ' ' , u.last_name) as full_name,
  u.email,
  a.street_address,
  a.city,
  a.country,
  a.postal_code,
  v.is_deleted

  FROM vehicles v
  JOIN models m ON  m.id =v.models_id
  JOIN brands b ON  b.id= m.brand_id
  JOIN users u ON u.id = v.customer_id
  JOIN addresses a ON a.id = u.addresses_id

  WHERE ${column} = ? and v.is_deleted = 0
  `;

  return await pool.query(sql, [value]);
};

const getByVin = async (value) => {
  const sql = `
  SELECT v.id,
  m.model_name as model,
  b.brand_name as brand,
  v.year,
  v.reg_plate,
  v.class,
  v.vin,
  v.transmissions,
  v.engine,
  v.is_deleted

  FROM vehicles v
  JOIN models m ON  m.id =v.models_id
  JOIN brands b ON  b.id= m.brand_id

  WHERE v.vin = ? 
  `;

  return await pool.query(sql, [value]);
};

const getById = async (id) => {
  const sql = `
    SELECT v.id,
    m.model_name as model,
    b.brand_name as brand,
    v.year,
    v.reg_plate,
    v.class,
    v.vin,
    v.transmissions,
    v.engine,
    concat(u.first_name, ' ' , u.last_name) as full_name,
    u.email,
    a.street_address,
    a.city,
    a.country,
    a.postal_code,
    v.is_deleted,
    v.customer_id,
    v.models_id

    FROM vehicles v
    JOIN models m ON  m.id =v.models_id
    JOIN brands b ON  b.id= m.brand_id
    JOIN users u ON u.id = v.customer_id
    JOIN addresses a ON a.id = u.addresses_id

    WHERE v.id = ? AND v.is_deleted = 0
    `;

  return await pool.query(sql, [+id]);
};

const update = async (id, data, modelId) => {
  const sql = `
    UPDATE vehicles v 
    LEFT JOIN models m ON m.id = v.models_id
    LEFT JOIN brands b ON b.id = m.brand_id
    
    SET v.reg_plate = ?,
    v.year = ?,
    v.class = ?,
    v.transmissions = ?,
    v.engine = ?,
    m.model_name = ?,
    v.models_id = ?
  
    WHERE v.id = ? 
    `;

  return await pool.query(sql, [
    data.reg_plate,
    data.year,
    data.class,
    data.transmissions,
    data.engine,
    data.model,
    modelId,
    +id,
  ]);
};

const deleted = async (id) => {
  const sql1 = `
  UPDATE vehicles  
  SET is_deleted = 1
  WHERE id = ? 
  `;

  await pool.query(sql1, [+id]);

  const sql2 = `
  SELECT v.id,
  m.model_name as model,
  b.brand_name as brand,
  v.year,
  v.reg_plate,
  v.class,
  v.vin,
  v.transmissions,
  v.engine,
  concat(u.first_name, ' ' , u.last_name) as full_name,
  u.email,
  a.street_address,
  a.city,
  a.country,
  a.postal_code,
  v.is_deleted

  FROM vehicles v
  JOIN models m ON  m.id =v.models_id
  JOIN brands b ON  b.id= m.brand_id
  JOIN users u ON u.id = v.customer_id
  JOIN addresses a ON a.id = u.addresses_id

  WHERE v.id = ? 
  `;

  return await pool.query(sql2, [+id]);
};

const searchByModelAndBrand = async (model, brand) => {
  const sql = `
  SELECT m.id , m.model_name as model, b.brand_name as brand
  FROM models m
  JOIN brands b ON  b.id= m.brand_id
  WHERE m.model_name = ? and b.brand_name = ?
  `;

  return await pool.query(sql, [model, brand]);
};

const createModel = async (data) => {
  const sql1 = `
  SELECT b.id
  FROM brands b
  WHERE b.brand_name = ?
  `;
  const brandId = await pool.query(sql1, [data.brand]);

  const sql2 = `
  INSERT INTO models (model_name, brand_id)
  VALUES (?, ?)
  `;
  return pool.query(sql2, [data.model, brandId[0].id]);
};

const create = async (data, modelId) => {
  const sql = `
  INSERT INTO vehicles (reg_plate, year, class, vin, transmissions, engine, models_id, customer_id)
  VALUES (?,?,?,?,?,?,?,?)
  `;
  return await pool.query(sql, [
    data.reg_plate,
    data.year,
    data.class,
    data.vin,
    data.transmissions,
    data.engine,
    modelId,
    data.customer_id,
  ]);
};

const searchModelBy = async (column, value) => {
  const sql = `
    SELECT b.id, b.brand_name as brand, m.model_name as model
   
    FROM models m
    JOIN brands b ON  b.id= m.brand_id

    WHERE ${column} = ? 
  `;

  return await pool.query(sql, [value]);
};

export default {
  searchByModelAndBrand,
  searchByQueries,
  searchModelBy,
  createModel,
  searchBy,
  getByVin,
  deleted,
  getById,
  update,
  create,
  getAll,
  getBy,
};
