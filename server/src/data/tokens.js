import pool from './pool.js';

export const tokenExist = async (token) => {
  const sql = `
  SELECT *
  FROM tokens
  WHERE token = ? 
  `;

  const result = await pool.query(sql, [token]);

  return result && result.length > 0;
};

const save = async (token, userId) => {
  const sql = `
  INSERT INTO reset_passwords (token, user_reset_id) 
  VALUES (?, ?)`;

  return await pool.query(sql, [token, userId]);
};

const checkExistingToken = async (token) => {
  const sql = `
  SELECT user_reset_id, expire_time
  FROM reset_passwords
  WHERE token = ?
  AND is_used=0
  `;

  const result = await pool.query(sql, [token]);
  return result[0];
};

const updateToken = async (token) => {
  const sql = `
  UPDATE reset_passwords SET is_used  = 1 WHERE token = ?;
  `;
  return await pool.query(sql, [token]);
};

export default {
  checkExistingToken,
  updateToken,
  tokenExist,
  save,
};
