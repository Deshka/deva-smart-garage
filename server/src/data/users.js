import {
  BASE_URL_AVATARS,
  BASE_URL_AVATARS_DEFAULT,
} from '../common/constants.js';
import { queryFuncSql, checkLengthSql } from '../common/helpers.js';
import pool from './pool.js';

const getAll = async (data) => {
  const direction = data.sort || 'ASC';
  const resultsPerPage = data.limit || 20;
  const startIndex = data.page ? (data.page - 1) * data.limit : 0;

  const sql = `
    SELECT u.id,
    u.first_name, 
    u.last_name, 
    concat(u.first_name, ' ' , u.last_name) as full_name,
    u.role, 
    u.email, 
    u.password, 
    u.phone_number,
    u.avatar,
    a.street_address,
    a.city,
    a.country,
    a.postal_code,
    u.is_deleted,
    v.year,
    v.reg_plate,
    v.class,
    v.vin,
    v.transmissions,
    v.engine,
    v.customer_id,
    v.id as vehicle_id,
    v.models_id,
    o.issue_date,
    o.due_date,
    m.model_name as model,
    b.brand_name as brand

    FROM users u
    LEFT JOIN addresses a on a.id = u.addresses_id
    LEFT JOIN vehicles v on v.customer_id = u.id
    LEFT JOIN models m ON  m.id =v.models_id
    LEFT JOIN brands b ON  b.id= m.brand_id
    LEFT JOIN orders o ON o.vehicle_id = v.id

    WHERE u.is_deleted = 0
    GROUP by u.id
    ORDER BY u.first_name ${direction}
    LIMIT ${startIndex}, ${resultsPerPage}
    `;

  return await pool.query(sql);
};

const searchByQueries = async (
    userQueries,
    vehicleQueries,
    brandQueries,
    modelQueries,
    orderQueries,
    data
) => {
  const direction = data.sort || 'ASC';
  const resultsPerPage = data.limit || 10;
  const startIndex = data.page ? (data.page - 1) * data.limit : 0;

  const sql1 = `
    SELECT u.id,
    u.first_name, 
    u.last_name, 
    u.role, 
    u.email, 
    u.password, 
    u.phone_number,
    u.avatar,
    a.street_address,
    a.city,
    a.country,
    a.postal_code,
    u.is_deleted,
    v.year,
    v.reg_plate,
    v.class,
    v.vin,
    v.transmissions,
    v.engine,
    v.customer_id,
    v.models_id,
    o.issue_date,
    o.due_date
    
    FROM users u
    JOIN addresses a on a.id = u.addresses_id
    JOIN vehicles v on v.customer_id = u.id
    JOIN models m ON  m.id =v.models_id
    JOIN brands b ON  b.id= m.brand_id
    JOIN orders o ON o.vehicle_id = v.id

    WHERE 
  `;

  const sql2Arr = queryFuncSql(userQueries, 'u');
  const sql2 = checkLengthSql(sql2Arr);

  const sql3Arr = queryFuncSql(vehicleQueries, 'v');
  const sql3 = checkLengthSql(sql3Arr);

  const sql4Arr = queryFuncSql(brandQueries, 'b');
  const sql4 = checkLengthSql(sql4Arr);

  const sql5Arr = queryFuncSql(modelQueries, 'm');
  const sql5 = checkLengthSql(sql5Arr);

  const sql6Arr = queryFuncSql(orderQueries, 'o');
  const sql6 = checkLengthSql(sql6Arr);

  const sqlQueries = sql1.concat(sql2, sql3, sql4, sql5, sql6).slice(0, -4);

  const sql7 = `ORDER BY u.first_name ${direction}
  LIMIT ${startIndex}, ${resultsPerPage}`;

  const sql = sqlQueries + sql7;

  return await pool.query(sql);
};

const getBy = async (column, value, user) => {
  const sql = `
    SELECT u.id,
    u.first_name, 
    u.last_name, 
    concat(u.first_name, ' ' , u.last_name) as full_name,
    u.role, 
    u.email, 
    u.password, 
    u.phone_number,
    u.avatar,
    u.addresses_id,
    a.street_address,
    a.city,
    a.country,
    a.postal_code,
    u.is_deleted

    FROM users u
    JOIN addresses a on a.id = u.addresses_id

    WHERE ${column} = ? AND u.is_deleted = 0
    `;

  return await pool.query(sql, [value]);
};

const searchBy = async (column, value) => {
  const sql = `
    SELECT u.id,
    u.first_name, 
    u.last_name, 
    u.role, 
    u.email, 
    u.password, 
    u.phone_number,
    u.avatar,
    a.street_address,
    a.city,
    a.country,
    a.postal_code,
    u.is_deleted,
    v.year,
    v.reg_plate,
    v.class,
    v.vin,
    v.transmissions,
    v.engine,
    v.customer_id,
    v.models_id,
    o.issue_date,
    o.due_date

    FROM users u
    JOIN addresses a on a.id = u.addresses_id
    JOIN vehicles v on v.customer_id = u.id
    JOIN models m ON  m.id =v.models_id
    JOIN brands b ON  b.id= m.brand_id
    JOIN orders o ON o.vehicle_id = v.id

    WHERE ${column} LIKE '%${pool.escape(value).replace(/'/g, '')}%' 
    AND is_deleted = 0
    `;

  return await pool.query(sql);
};

const create = async (data, avatar) => {
  let profileImage = `${BASE_URL_AVATARS_DEFAULT}`;

  if (avatar && avatar.filename) {
    profileImage = `${BASE_URL_AVATARS}${avatar.filename}`;
  }

  const sql = `
    INSERT INTO users (first_name, last_name, email, password, phone_number, addresses_id, avatar)
    VALUES (?,?,?,?,?,?,?)
  `;
  const createdUser = await pool.query(sql, [
    data.first_name,
    data.last_name,
    data.email,
    data.password,
    data.phone_number,
    data.addresses_id,
    profileImage,
  ]);

  return await getBy('u.id', createdUser.insertId);
};

const getAddressById = async (id) => {
  const sql = `
    SELECT a.id,
    a.street_address,
    a.city,
    a.country,
    a.postal_code

    FROM addresses a
    WHERE a.id = ?
  `;

  return await pool.query(sql, [+id]);
};

const createAddress = async (data, user) => {
  const sql = `
    INSERT INTO addresses ( street_address, city, country, postal_code)
    VALUES (?,?,?,?)
  `;

  const address = await pool.query(sql, [
    data.street_address,
    data.city,
    data.country,
    data.postal_code,
  ]);

  return await getAddressById(address.insertId);
};

const deleted = async (id) => {
  const sql = `
    UPDATE users u
    SET u.is_deleted = 1
    WHERE u.id = ?
  `;

  await pool.query(sql, [+id]);
  return await getBy('u.id', +id);
};

const update = async (data, id, avatar, user) => {
  let profileImage = `${BASE_URL_AVATARS_DEFAULT}`;

  if (avatar && avatar.filename) {
    profileImage = `${BASE_URL_AVATARS}${avatar.filename}`;
  }

  const sql = `
  UPDATE users u 
  LEFT JOIN addresses a ON a.id = u.addresses_id
  
  SET u.first_name = ?,
  u.last_name = ?,
  u.email = ?,
  u.phone_number = ?,
  a.street_address = ?,
  a.city = ?,
  a.country = ?,
  a.postal_code = ?,
  u.avatar = ?

  WHERE u.id = ? 
  `;

  return await pool.query(sql, [
    data.first_name,
    data.last_name,
    data.email,
    data.phone_number,
    data.street_address,
    data.city,
    data.country,
    data.postal_code,
    profileImage,
    +id,
  ]);
};

const checkDeleted = async (email) => {
  const sql = `
  SELECT id, is_deleted, email
  FROM users
  WHERE (email = ? AND is_deleted = 1)
  `;

  return await pool.query(sql, [email]);
};

const logoutUser = async (token) => {
  const sql = `
  INSERT INTO tokens (token) VALUES (?)
  `;

  return await pool.query(sql, [token]);
};

const updateUserPassword = async (id, password) => {
  const sql = `
  UPDATE users SET password = ? WHERE id = ?`;
  return await pool.query(sql, [password, id]);
};

export default {
  updateUserPassword,
  searchByQueries,
  getAddressById,
  createAddress,
  checkDeleted,
  logoutUser,
  searchBy,
  deleted,
  update,
  create,
  getAll,
  getBy,
};
