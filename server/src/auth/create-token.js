import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

const PRIVATE_KEY = dotenv.config().parsed.PRIVATE_KEY;

const createToken = (payload) => {
  const token = jwt.sign(
      payload,
      PRIVATE_KEY,
  );

  return token;
};

export default createToken;
