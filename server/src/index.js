import vehiclesController from './controllers/vehicles-controller.js';
import ordersController from './controllers/orders-controller.js';
import usersController from './controllers/users-controller.js';
import authController from './controllers/auth-controller.js';
import servicesController from './controllers/service-controller.js';
import jwtStrategy from './auth/strategy.js';
import passport from 'passport';
import express from 'express';
import dotenv from 'dotenv';
import helmet from 'helmet';
import cors from 'cors';

const app = express();

const config = dotenv.config().parsed;
const PORT = config.PORT;

passport.use(jwtStrategy);
app.use(cors());

app.use(passport.initialize());
app.use(helmet());
app.use(express.json());

app.use('/avatars', express.static('avatars'));
app.use('/', authController);

app.use('/', vehiclesController);

app.use('/', ordersController);

app.use('/', usersController);

app.use('/', servicesController);
app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' })
);

app.listen(PORT, console.log(`Listening on port ${PORT}...`));
