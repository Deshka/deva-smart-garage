import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import validateUpdateStatus from '../validators/update-status-scheme.js';
import validateUpdateOrder from '../validators/update-order-scheme.js';
import createOrderSchema from '../validators/create-order-scheme.js';
import loggerUserGuard from '../middleware/loggerUserGuard.js';
import ordersService from '../services/orders-service.js';
import serviceErrors from '../services/service-errors.js';
import validateBody from '../middleware/validate-body.js';
import { userRole } from '../common/user-role.js';
import vehicleData from '../data/vehicles.js';
import ordersData from '../data/orders.js';
import express from 'express';

const ordersController = express.Router();

ordersController
// for employee all orders
    .get('/orders', authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee), async (req, res) => {
      const { error, order } = await ordersService.getAllOrders(ordersData)();
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Order is not found!', error: error });
      } else {
        res.status(200).send( order );
      }
    })
// for logged user to get his orders
    .get('/user/order', authMiddleware, loggerUserGuard, async (req, res) => {
      const { id } = req.user;
      const data = req.query;
      const { error, order } = await ordersService.getByUser(ordersData)(id, data);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'No Orders found for the user!', error: error });
      } else if (error === serviceErrors.NO_MATCHES) {
        res.status(404).send({ message: 'Orders with such queries not found!', error: error });
      } else {
        res.status(200).send(order);
      }
    })
// for employee to detele users order by order id
    .delete('/user/order/:ordId', authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee), async (req, res) => {
      const data = req.params;
      const { error, order } = await ordersService.deleteOrder(ordersData)(data);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Order is not found!', error: error });
      } else {
        res.status(200).send({ message: 'Order is deleted!', order});
      }
    })
// for employee to add services to order
    .put('/user/order/:ordId', authMiddleware, loggerUserGuard, validateBody('order', validateUpdateOrder), roleMiddleware(userRole.Employee), async (req, res) => {
      const data = req.params;
      const dataBody = req.body;
      const { error, order } = await ordersService.updateOrder(ordersData)(data, dataBody);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Order is not found!', error: error });
      } else if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(409).send({ message: 'Service already added!', error: error });
      } else {
        res.status(200).send({ message: 'Service is added to order', order });
      }
    })
    // for employee to update order status
    .put('/order/:ordId/status', authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee), validateBody('order', validateUpdateStatus), async (req, res) => {
      const data = req.params;
      const dataBody = req.body;
      const { error, order } = await ordersService.updateOrderStatus(ordersData)(data, dataBody);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Order is not found!', error: error });
      } else if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(409).send({ message: 'Service already added!', error: error });
      } else {
        res.status(200).send( order );
      }
    })
// for employee to create order for already created user  and vehicle
    .post('/order/vehicle/:id', authMiddleware, loggerUserGuard, roleMiddleware(userRole.Employee), validateBody('order', createOrderSchema), async (req, res) => {
      const { id } = req.params;
      const data = { 'user': req.user.id, id };
      const dataBody = req.body;
      const { error, order } = await ordersService.createOrder(ordersData, vehicleData)(data, dataBody);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Vehicle is not found!', error: error });
      } else {
        res.status(200).send( order );
      }
    });
export default ordersController;
