import loggerUserGuard from '../middleware/loggerUserGuard.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import updateVehicleScheme from '../validators/update-vehicle-scheme.js';
import createVehicleScheme from '../validators/create-vehicle-scheme.js';
import vehiclesService from '../services/vehicles-service.js';
import serviceErrors from '../services/service-errors.js';
import validateBody from '../middleware/validate-body.js';
import vehiclesData from '../data/vehicles.js';
import express from 'express';
import { userRole } from '../common/user-role.js';

const vehiclesController = express.Router();

vehiclesController.get('/vehicles',
    authMiddleware,
    loggerUserGuard,
    roleMiddleware(userRole.Employee),

    async (req, res) => {
      const data = req.query;
      const { error, vehicles } = await vehiclesService.getAllVehicles(
          vehiclesData
      )( data );

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Vehicles not found!', error: error });
      } else if (error === serviceErrors.NO_MATCHES) {
        res.status(404).send({ message: 'Vehicles with such queries not found!', error: error });
      } else {
        res.status(200).send(vehicles);
      }
    });

vehiclesController.get('/vehicles/:id',
    authMiddleware,
    loggerUserGuard,
    async (req, res) => {
      const { id } = req.params;

      const { error, vehicle } = await vehiclesService.getVehicleById(
          vehiclesData
      )(id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'vehicle not found!', error: error });
      } else {
        res.status(200).send(vehicle);
      }
    });

vehiclesController.get('/users/:id/vehicles',
    authMiddleware,
    loggerUserGuard,
    async (req, res) => {
      const { id } = req.params;

      const { error, vehicle } = await vehiclesService.getVehiclesByUserId(
          vehiclesData
      )(id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'vehicle not found!', error: error });
      } else {
        res.status(200).send(vehicle);
      }
    });

vehiclesController.put('/vehicles/:id',
    authMiddleware,
    loggerUserGuard,
    roleMiddleware(userRole.Employee),
    validateBody('vehicle', updateVehicleScheme),

    async (req, res) => {
      const { id } = req.params;
      const data = req.body;
      const user = req.user;
      const { error, vehicle } = await vehiclesService.updateVehicle(
          vehiclesData
      )(id, data, user);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'vehicle not found!', error: error });
      } else {
        res.status(200).send(vehicle);
      }
    });

vehiclesController.post('/vehicles',
    authMiddleware,
    loggerUserGuard,
    roleMiddleware(userRole.Employee),
    validateBody('vehicle', createVehicleScheme),

    async (req, res) => {
      const data = req.body;
      const user = req.user;
      const { error, vehicle } = await vehiclesService.createVehicle(
          vehiclesData
      )( data, user);

      if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(404).send({ message: 'Vehicle with such a vin already exist!', error: error });
      } else {
        res.status(200).send(vehicle);
      }
    });

vehiclesController.delete('/vehicles/:id',
    authMiddleware,
    loggerUserGuard,
    roleMiddleware(userRole.Employee),

    async (req, res) => {
      const user = req.user;
      const { id } = req.params;
      const { error, vehicle } = await vehiclesService.deleteVehicle(
          vehiclesData
      )(id, user);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'vehicle not found or already is deleted!', error: error });
      } else {
        res.status(200).send(vehicle);
      }
    });

export default vehiclesController;
