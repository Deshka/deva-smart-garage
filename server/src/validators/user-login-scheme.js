import {
  validateString,
  validateLength,
  isValid,
  emailValidator,
} from '../common/validators.js';

export default {
  email: (value) =>
    isValid(value) &&
    validateString(value) &&
    validateLength(value, 3, 100) &&
    emailValidator(value),

  password: (value) =>
    isValid(value) && validateString(value) && validateLength(value, 8, 20),
};
