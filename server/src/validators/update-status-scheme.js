import { statusValidator } from '../common/validators.js';

export default {
  status: (value) => statusValidator(value),
};
