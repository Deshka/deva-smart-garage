import { validateNumber } from '../common/validators.js';

export default {
  price: (value) => validateNumber(value),
};
