import {
  validateString,
  validateLength,
  isValidDate,
  statusValidator,
} from '../common/validators.js';

export default {
  due_date: (value) => isValidDate(value),

  issue_date: (value) => isValidDate(value),

  notes: (value) => validateString(value) && validateLength(value, 3, 50),

  status: (value) => statusValidator(value),
};
