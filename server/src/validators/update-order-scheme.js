import { validateNumber } from '../common/validators.js';

export default {
  service_id: (value) => value === undefined || validateNumber(value),
};
