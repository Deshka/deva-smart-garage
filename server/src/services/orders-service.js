import serviceErrors from './service-errors.js';
import { filterQueries } from '../common/helpers.js';

const getAllOrders = (ordersData) => {
  return async () => {
    const order = await ordersData.getAll();
    if (!order[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }
    return { error: null, order: [...order] };
  };
};

const getByUser = (ordersData) => {
  return async (id, data) => {
    const filter = Object.keys(data).reduce((acc, query, index, arr) => {
      if (query !== 'limit' && query !== 'sort' && query !== 'page') {
        acc.push(query);
      }
      return acc;
    }, []);

    if (filter.length) {
      const orderQueries = filterQueries(data, 'due_date', 'issue_date');
      const result = await ordersData.searchByQueries(
          'customer_id',
          id,
          data,
          orderQueries
      );
      if (!result[0]) {
        return { error: serviceErrors.NO_MATCHES, service: null };
      }
      return { error: null, order: result[0] };
    }
    const order = await ordersData.getBy('customer_id', id);

    if (!order[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }

    return { error: null, order: order };
  };
};

const deleteOrder = (ordersData) => {
  return async (data) => {
    const order = await ordersData.getOrder(data);
    if (!order[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }
    const del = await ordersData.deleteOrder(data.ordId);
    const deletedOrder = await ordersData.getOrder(data);
    return { error: null, order: deletedOrder };
  };
};

const updateOrder = (ordersData) => {
  return async (data, dataBody) => {
    const order = await ordersData.getOrder(data);
    if (!order[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }
    if (dataBody.service_id) {
      const services = await ordersData.getServices(
          data.ordId,
          dataBody.service_id
      );

      if (services[0]) {
        return {
          error: serviceErrors.DUPLICATE_RECORD,
          order: null,
        };
      }
      const addService = await ordersData.addService(
          data.ordId,
          dataBody.service_id
      );
    }
    const updated = await ordersData.getOrder(data);
    return { error: null, order: updated };
  };
};

const updateOrderStatus = (ordersData) => {
  return async (data, dataBody) => {
    const order = await ordersData.getOrder(data);
    if (!order[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }
    const update = await ordersData.update(dataBody, data.ordId);
    const updated = await ordersData.getOrder(data);
    return { error: null, order: updated };
  };
};
const createOrder = (ordersData, vehicleData) => {
  return async (data, dataBody) => {
    const vehicle = await vehicleData.getById(data.id);
    if (!vehicle[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }
    const create = await ordersData.create(data, dataBody);
    const created = await ordersData.getCreated(create.insertId);
    return { error: null, order: created };
  };
};
export default {
  getAllOrders,
  getByUser,
  deleteOrder,
  updateOrder,
  createOrder,
  updateOrderStatus,
};
