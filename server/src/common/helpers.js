import pool from '../data/pool.js';

export const queryFuncSql = (arr, table) => {
  return arr.map(([key, value]) => `${table}.${key} LIKE '%${pool.escape(value).replace(/'/g, '')}%'`).join(' AND ');
};

export const checkLengthSql = (arr) => {
  return arr.length > 0 ? `${arr} AND ` : ``;
};

export const filterQueries = (data, ...params) => {
  return Object.entries(data).filter((el)=>{
    return params.some((e)=>el[0] === e);
  });
};
