export const validateString = (value) => typeof value === 'string';

// (359)-882-974963, 541-753-6010, 541753-6010, or 541753-6010.
// 359882974963, 359-882-974963, +359882974963, +359-882-974963
export const phoneValidator = (value) => {
  const regex = /^\(?[+]?(\d{3})\)?[- ]?(\d{3})[- ]?(\d+)$/g;
  return regex.test(value);
};

// vehicle identification number
export const vinValidator = (value) => {
  const regex = /^[a-zA-Z0-9]{9}[a-zA-Z0-9-]{2}[0-9]{6}$/gm;
  return regex.test(value);
};

// vehicle license plate
export const licensePlateValidator = (value) => {
  const regex = /^([A-Z]){1||2}([0-9]){4}([A-Z]){2}$/gm;
  return regex.test(value);
};

// vehicle year - 1900 - 2099
export const yearValidator = (value) => {
  const regex = /^(19|20)\d{2}$/gm;
  return regex.test(value);
};

// vehicle class
export const classValidator = (value) => {
  const regex = /^[A,B,C,D,E,F,S]{1}$/gm;
  return regex.test(value);
};

// vehicle transmissions
export const transmissionsValidator = (value) => {
  return ['manual', 'auto'].includes(value);
};

// vehicle engine
export const engineValidator = (value) => {
  return ['petrol', 'diesel', 'hybrid'].includes(value);
};

// string length - used for brand and model vehicle validation
export const validateLength = (value, min, max) => {
  return value.length >= min && value.length <= max;
};

// valid brand
export const isValidBrand = (value) => {
  return [
    'Seat',
    'Renault',
    'Peugeot',
    'Dacia',
    'Citroën',
    'Opel',
    'Alfa Romeo',
    'Škoda',
    'Chevrolet',
    'Porsche',
    'Honda',
    'Subaru',
    'Mazda',
    'Mitsubishi',
    'Lexus',
    'Toyota',
    'BMW',
    'Volkswagen',
    'Suzuki',
    'Mercedes-Benz',
    'Saab',
    'Audi',
    'Kia',
    'Land Rover',
    'Dodge',
    'Chrysler',
    'Ford',
    'Hummer',
    'Hyundai',
    'Infiniti',
    'Jaguar',
    'Jeep',
    'Nissan',
    'Volvo',
    'Tewoo',
    'Fiat',
    'MINI',
    'Rover',
    'Smart',
  ].includes(value);
};

export const emailValidator = (value) => {
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g;
  return regex.test(value);
};

// post_code validator
export const postCodeValidator = (value) => {
  const regex = /^([1-9]){1}([0-9]){3}$/gm;
  return regex.test(value);
};

export const statusValidator = (value) => {
  return ['new', 'processing', 'done'].includes(value);
};
export const isValid = (value) => value;
export const isValidDate = (value) =>
  !new Date(value).toString().includes('Invalid');

export const validateNumber= (value) => typeof value === 'number';
